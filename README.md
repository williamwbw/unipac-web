# README #

This document readme is for Unipac Web Application projects

### What is this repository for? ###

* Documentation & source code

### How do I get set up? ###

### Tools to use ###
1: Visual Studio Code

2: Visual Studio Community (2019)

3: SQL Server Developer 2019

4: Microsoft SQL Server Management Studio 18


### Setup for Environment ###
1: Click On "Windows" Button to search windows features

2: Tick on .NetFramework 4.8 Advanced Services (Tick All)

3: Tick On Internet Information Services & Internet Information Services Hostable Web Core (Tick ALL)

4: Open IIS 7, right click on the side, to add new application
	
	A: For API, Please use following web.config
	<?xml version="1.0" encoding="utf-8"?>
		<configuration>
		  <!-- To customize the asp.net core module uncomment and edit the following section. 
		  For more info see https://go.microsoft.com/fwlink/?linkid=838655 -->
		  <system.webServer>
			<handlers>
			  <add name="aspNetCore" path="*" verb="*" modules="AspNetCoreModuleV2" resourceType="Unspecified" />
			</handlers>
			<aspNetCore processPath="CMU_CRUD.exe" arguments="%LAUNCHER_ARGS%" stdoutLogEnabled="true" stdoutLogFile=".\logs\stdout" forwardWindowsAuthToken="false" hostingModel="InProcess" />
		  </system.webServer>
		</configuration>
	B: For Web Portal Please use following web.config
	<?xml version="1.0" encoding="UTF-8"?>
	<configuration>
		  <system.webServer>
			  <rewrite>
				  <rules>
					  <rule name="Unipac CMU" stopProcessing="true">
						  <match url="(.*)" />
						  <conditions logicalGrouping="MatchAll">
							<add input="{REQUEST_URI}" pattern="^/api/.*" negate="true" />
							<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
							<add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
						  </conditions>
						  <action type="Rewrite" url="/index.html" />
					  </rule>
				  </rules>
			  </rewrite>
		  </system.webServer>
	</configuration>

Note: if in development mode (LOCAL PC), just require to run the visual studio 2019 in debug mode, API will be running as well, for web portal, please use npm run serve

### VSC extension for vuejs

### Name: vue

Id: jcbuisson.vue

Description: Syntax Highlight for Vue.js

Version: 0.1.5


Publisher: jcbuisson

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=jcbuisson.vue

### Name: Vue 3 Snippets

Id: hollowtree.vue-snippets

Description: A Vue.js 3 And Vue.js 2 Code Snippets Extension

Version: 1.0.4

Publisher: hollowtree

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=hollowtree.vue-snippets

### Name: Vue VSCode Snippets

Id: sdras.vue-vscode-snippets

Description: Snippets that will supercharge your Vue workflow

Version: 2.2.1

Publisher: sarah.drasner

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets

### Name: vue-beautify

Id: peakchen90.vue-beautify

Description: Beautify Vue code in place for vscode

Version: 2.0.4

Publisher: peakchen90

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=peakchen90.vue-beautify

### Name: Vue VS Code Extension Pack

Id: sdras.vue-vscode-extensionpack

Description: A collection of extensions for working with Vue Applications in VS Code

Version: 0.2.0

Publisher: sarah.drasner

VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-extensionpack


### Vue JS Installation ###
Step 1: Download Node.js Installer (https://nodejs.org/en/download/)

Step 2: Install Node.js and NPM from Browser

1. Once the installer finishes downloading, launch it. Open the downloads link in your browser and click the file. Or, browse to the location where you have saved the file and double-click it to launch.

2. The system will ask if you want to run the software – click Run.

3. You will be welcomed to the Node.js Setup Wizard – click Next.

4. On the next screen, review the license agreement. Click Next if you agree to the terms and install the software.

5. The installer will prompt you for the installation location. Leave the default location, unless you have a specific need to install it somewhere else – then click Next.

6. The wizard will let you select components to include or remove from the installation. Again, unless you have a specific need, accept the defaults by clicking Next.

7. Finally, click the Install button to run the installer. When it finishes, click Finish.

Step 3: Verify Installation
Open a command prompt (or PowerShell), and enter the following:

node -v
The system should display the Node.js version installed on your system. You can do the same for NPM:

npm -v
Testing Node JS and NPM on Windows using CMD

Step 4: npm install vue

Step 5: npm install -g @vue/cli

### After open the PROJECT of VUEJS, please open up the terminal, and type in command [ NPM i ], this is to install all extension used in selected project.

Command to run vue project for development = npm run serve

Command to build vue project for production = npm run build