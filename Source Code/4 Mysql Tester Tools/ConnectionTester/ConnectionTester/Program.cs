﻿using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ConnectionTester
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            while (true)
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                await function2Async();
                watch.Stop();
                Console.WriteLine("Execute Time : " + watch.Elapsed.TotalSeconds);
                Thread.Sleep(3000);
            }
        }

        public static async System.Threading.Tasks.Task function2Async()
        {
            var sshServer = "115.134.118.10";
            var sshUserName = "ADMIN";
            var sshPassword = "";
            var databaseServer = "DESKTOP-LTUUHOK";
            var databaseUserName = "newuser";
            var databasePassword = "Unipac@123";
            var sshPort = 5002;
            var sshKeyFile = "C:\\Users\\William\\Desktop\\id_rsa";
            Console.WriteLine("Running Time: " + DateTime.Now);
            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = "qmax_status",
                        //ConnectionTimeout = 3
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    var connection = new MySqlConnection(csb.ConnectionString);
                    if (localPort > 0)
                    {
                        await connection.OpenAsync();
                        string Query = string.Format(@"SELECT * FROM tblmachineinfo where cFax <> 'OFF'");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        while (await reader.ReadAsync())
                        {
                            Console.WriteLine(reader.GetString("cMachineID") + " Status " + reader.GetString("cFax")) ;
                        }
                        connection.Close();
                    }
                    else
                    {
                        Console.WriteLine("No Connection");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);

            }
        }

        public static (SshClient SshClient, uint Port) ConnectSsh(string sshHostName, string sshUserName, string sshPassword = null,
string sshKeyFile = "C:\\Users\\William\\Desktop\\id_rsa", string sshPassPhrase = null, int sshPort = 5003, string databaseServer = "localhost", int databasePort = 3306)
        {
            //WriteToFile(DateTime.Now + ": SSHClient Processing...");
            // check arguments
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshHostName)} must be specified.", nameof(sshHostName));
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshUserName)} must be specified.", nameof(sshUserName));
            if (string.IsNullOrEmpty(sshPassword) && string.IsNullOrEmpty(sshKeyFile))
                throw new ArgumentException($"One of {nameof(sshPassword)} and {nameof(sshKeyFile)} must be specified.");
            if (string.IsNullOrEmpty(databaseServer))
                throw new ArgumentException($"{nameof(databaseServer)} must be specified.", nameof(databaseServer));

            // define the authentication methods to use (in order)
            var authenticationMethods = new List<AuthenticationMethod>();
            try
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...KeyFile: " + sshKeyFile);
                if (!string.IsNullOrEmpty(sshKeyFile))
                {
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshUserName: " + sshUserName);
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshPassPhrase: " + sshPassPhrase);
                    authenticationMethods.Add(new PrivateKeyAuthenticationMethod(sshUserName,
                        new PrivateKeyFile(sshKeyFile, string.IsNullOrEmpty(sshPassPhrase) ? null : sshPassPhrase)));
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...Done: " + authenticationMethods);
                }
            }
            catch (Exception ex)
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
            }
            try
            {
                if (!string.IsNullOrEmpty(sshPassword))
                {
                    authenticationMethods.Add(new PasswordAuthenticationMethod(sshUserName, sshPassword));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Password: " + ex.InnerException.Message);
            }

            // connect to the SSH server
            var sshClient = new SshClient(new ConnectionInfo(sshHostName, sshPort, sshUserName, authenticationMethods.ToArray()));
            sshClient.ConnectionInfo.Timeout = TimeSpan.FromSeconds(3);
            try
            {
                sshClient.Connect();

                // forward a local port to the database server and port, using the SSH server
                var forwardedPort = new ForwardedPortLocal("127.0.0.1", databaseServer, (uint)databasePort);
                sshClient.AddForwardedPort(forwardedPort);
                forwardedPort.Start();
                return (sshClient, forwardedPort.BoundPort);
            }
            catch(Exception ex)
            {
                return (sshClient, 0);
            }
        }
    }
}
