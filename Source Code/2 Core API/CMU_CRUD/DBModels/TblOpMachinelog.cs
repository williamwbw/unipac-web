﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpMachinelog
    {
        public int Id { get; set; }
        public int MachineId { get; set; }
        public string EventLog { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUserId { get; set; }
    }
}
