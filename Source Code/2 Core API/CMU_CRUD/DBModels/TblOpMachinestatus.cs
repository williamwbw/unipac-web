﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpMachinestatus
    {
        public int Id { get; set; }
        public int MachineId { get; set; }
        public string _12voltmeter { get; set; }
        public string _24voltmeter { get; set; }
        public string _12ammeter { get; set; }
        public string _24ammeter { get; set; }
        public string HourRun { get; set; }
        public string AirPressure { get; set; }
        public string Tempreature { get; set; }
        public string LowPressure { get; set; }
        public string Estop { get; set; }
        public DateTime CollectDateTime { get; set; }
        public string RecordStatus { get; set; }
    }
}
