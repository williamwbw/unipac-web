﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblCmAccessright
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int OperationLevel { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
