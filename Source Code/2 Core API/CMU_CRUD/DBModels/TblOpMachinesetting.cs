﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpMachinesetting
    {
        public int Id { get; set; }
        public decimal _12vrange { get; set; }
        public decimal _24vrange { get; set; }
        public decimal LowPressure { get; set; }
        public decimal Estop { get; set; }
        public decimal AirPressure { get; set; }
        public decimal Temperature { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
