﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblCmRatesetting
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int PassRate { get; set; }
        public int FailRate { get; set; }
        public int RetestRate { get; set; }
        public int RejectRate { get; set; }
        public int NoGloveRate { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
