﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewDaMachineStatus
    {
        public int CompanyId { get; set; }
        public int? Off { get; set; }
        public int? Production { get; set; }
        public int? Standby { get; set; }
    }
}
