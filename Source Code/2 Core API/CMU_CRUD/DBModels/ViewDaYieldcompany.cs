﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewDaYieldcompany
    {
        public string Type { get; set; }
        public int CompanyId { get; set; }
        public decimal _0 { get; set; }
        public decimal _1 { get; set; }
        public decimal _2 { get; set; }
        public decimal _3 { get; set; }
        public decimal _4 { get; set; }
        public decimal _5 { get; set; }
        public decimal _6 { get; set; }
        public decimal _7 { get; set; }
        public decimal _8 { get; set; }
        public decimal _9 { get; set; }
        public decimal _10 { get; set; }
        public decimal _11 { get; set; }
        public decimal _12 { get; set; }
    }
}
