﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewDaMeterrunning
    {
        public TimeSpan? Hourly { get; set; }
        public DateTime? Date { get; set; }
        public int MachineId { get; set; }
        public decimal? _12volt { get; set; }
        public decimal? _24volt { get; set; }
        public decimal? _12amm { get; set; }
        public decimal? _24amm { get; set; }
        public decimal? AirPressure { get; set; }
        public decimal? Tempreature { get; set; }
    }
}
