﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpMachineinfo
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string SerialNumber { get; set; }
        public string Location { get; set; }
        public string YearOfManufac { get; set; }
        public string Installationdate { get; set; }
        public string Calibrationdate { get; set; }
        public string Version { get; set; }
        public string Sshipaddress { get; set; }
        public string Sshport { get; set; }
        public string Sshusername { get; set; }
        public string Databaseserver { get; set; }
        public string Databasename { get; set; }
        public string Databaseusername { get; set; }
        public string Databasepassword { get; set; }
        public string Recordstatus { get; set; }
        public int Createuserid { get; set; }
        public DateTime Createdate { get; set; }
        public int Modifyuserid { get; set; }
        public DateTime Modifydate { get; set; }
    }
}
