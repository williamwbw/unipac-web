﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpLocalCmu
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int LocalCmu { get; set; }
        public string Sshipaddress { get; set; }
        public string Sshport { get; set; }
        public string SshuserName { get; set; }
        public string Dbserver { get; set; }
        public string Dbname { get; set; }
        public string DbuserName { get; set; }
        public string Dbpassword { get; set; }
        public string RecordStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        public int ModifyUserId { get; set; }
    }
}
