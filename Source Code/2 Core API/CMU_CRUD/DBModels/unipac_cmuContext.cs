﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class unipac_cmuContext : DbContext
    {
        public unipac_cmuContext()
        {
        }

        public unipac_cmuContext(DbContextOptions<unipac_cmuContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCmAccessright> TblCmAccessrights { get; set; }
        public virtual DbSet<TblCmAuditlog> TblCmAuditlogs { get; set; }
        public virtual DbSet<TblCmEmployee> TblCmEmployees { get; set; }
        public virtual DbSet<TblCmRatesetting> TblCmRatesettings { get; set; }
        public virtual DbSet<TblCmUser> TblCmUsers { get; set; }
        public virtual DbSet<TblOpCalibration> TblOpCalibrations { get; set; }
        public virtual DbSet<TblOpCompany> TblOpCompanies { get; set; }
        public virtual DbSet<TblOpLicense> TblOpLicenses { get; set; }
        public virtual DbSet<TblOpLocalCmu> TblOpLocalCmus { get; set; }
        public virtual DbSet<TblOpMachinefaultlog> TblOpMachinefaultlogs { get; set; }
        public virtual DbSet<TblOpMachineinfo> TblOpMachineinfos { get; set; }
        public virtual DbSet<TblOpMachinelog> TblOpMachinelogs { get; set; }
        public virtual DbSet<TblOpMachinesetting> TblOpMachinesettings { get; set; }
        public virtual DbSet<TblOpMachinestatus> TblOpMachinestatuses { get; set; }
        public virtual DbSet<TblOpProduction> TblOpProductions { get; set; }
        public virtual DbSet<TblOpProductiondetail> TblOpProductiondetails { get; set; }
        public virtual DbSet<TblOpProductsetting> TblOpProductsettings { get; set; }
        public virtual DbSet<TblOpReport> TblOpReports { get; set; }
        public virtual DbSet<TblOpRfid> TblOpRfids { get; set; }
        public virtual DbSet<TblOpVersion> TblOpVersions { get; set; }
        public virtual DbSet<Temp> Temps { get; set; }
        public virtual DbSet<ViewCmUser> ViewCmUsers { get; set; }
        public virtual DbSet<ViewDaFailed> ViewDaFaileds { get; set; }
        public virtual DbSet<ViewDaMachineStatus> ViewDaMachineStatuses { get; set; }
        public virtual DbSet<ViewDaMeterrunning> ViewDaMeterrunnings { get; set; }
        public virtual DbSet<ViewDaNoglove> ViewDaNogloves { get; set; }
        public virtual DbSet<ViewDaPassed> ViewDaPasseds { get; set; }
        public virtual DbSet<ViewDaReject> ViewDaRejects { get; set; }
        public virtual DbSet<ViewDaRetest> ViewDaRetests { get; set; }
        public virtual DbSet<ViewDaTotal> ViewDaTotals { get; set; }
        public virtual DbSet<ViewDaYieldcompany> ViewDaYieldcompanies { get; set; }
        public virtual DbSet<ViewOpBatchanalysis> ViewOpBatchanalyses { get; set; }
        public virtual DbSet<ViewOpBatchlist> ViewOpBatchlists { get; set; }
        public virtual DbSet<ViewOpMachineviewcompany> ViewOpMachineviewcompanies { get; set; }
        public virtual DbSet<ViewOpProductioninfo> ViewOpProductioninfos { get; set; }
        public virtual DbSet<ViewOpRfidinfo> ViewOpRfidinfos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=WIN2019-UNIPAC\\SQLEXPRESS;Database=unipac_cmu;User ID=Dev;Password=p@ssw0rd;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<TblCmAccessright>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_cm_accessright");

                entity.HasIndex(e => e.Id, "ID");

                entity.Property(e => e.CreateDate)
                    .HasPrecision(0)
                    .HasDefaultValueSql("('0000-00-00 00:00:00')");

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ModifyDate)
                    .HasPrecision(0)
                    .HasDefaultValueSql("('0000-00-00 00:00:00')");

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");
            });

            modelBuilder.Entity<TblCmAuditlog>(entity =>
            {
                entity.ToTable("tbl_cm_auditlog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Activity).IsRequired();

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TblCmEmployee>(entity =>
            {
                entity.ToTable("tbl_cm_employee");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.Email).IsRequired();

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasColumnName("EmployeeID");

                entity.Property(e => e.EmployeeName).IsRequired();

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.RecordStatus).IsRequired();
            });

            modelBuilder.Entity<TblCmRatesetting>(entity =>
            {
                entity.ToTable("tbl_cm_ratesetting");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('1')");
            });

            modelBuilder.Entity<TblCmUser>(entity =>
            {
                entity.ToTable("tbl_cm_users");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Username).IsRequired();
            });

            modelBuilder.Entity<TblOpCalibration>(entity =>
            {
                entity.ToTable("tbl_op_calibration");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("File_Name");

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnName("File_Path");

                entity.Property(e => e.MachineId)
                    .IsRequired()
                    .HasColumnName("MachineID");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Note).IsRequired();

                entity.Property(e => e.RecordStatus).IsRequired();
            });

            modelBuilder.Entity<TblOpCompany>(entity =>
            {
                entity.ToTable("tbl_op_company");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyAddress).IsRequired();

                entity.Property(e => e.CompanyName).IsRequired();

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblOpLicense>(entity =>
            {
                entity.ToTable("tbl_op_license");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("File_Name");

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnName("File_Path");

                entity.Property(e => e.MachineId)
                    .IsRequired()
                    .HasColumnName("MachineID");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.Note).IsRequired();

                entity.Property(e => e.RecordStatus).IsRequired();
            });

            modelBuilder.Entity<TblOpLocalCmu>(entity =>
            {
                entity.ToTable("tbl_op_LocalCMU");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.Dbname).HasColumnName("DBName");

                entity.Property(e => e.Dbpassword).HasColumnName("DBPassword");

                entity.Property(e => e.Dbserver).HasColumnName("DBServer");

                entity.Property(e => e.DbuserName).HasColumnName("DBUserName");

                entity.Property(e => e.LocalCmu).HasColumnName("LocalCMU");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.RecordStatus).IsRequired();

                entity.Property(e => e.Sshipaddress).HasColumnName("SSHIPAddress");

                entity.Property(e => e.Sshport).HasColumnName("SSHPort");

                entity.Property(e => e.SshuserName).HasColumnName("SSHUserName");
            });

            modelBuilder.Entity<TblOpMachinefaultlog>(entity =>
            {
                entity.ToTable("tbl_op_machinefaultlog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.Event).IsRequired();

                entity.Property(e => e.FaultLog).IsRequired();

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<TblOpMachineinfo>(entity =>
            {
                entity.ToTable("tbl_op_machineinfo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Calibrationdate)
                    .IsRequired()
                    .HasColumnName("calibrationdate");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.Createdate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdate");

                entity.Property(e => e.Createuserid).HasColumnName("createuserid");

                entity.Property(e => e.Databasename)
                    .IsRequired()
                    .HasColumnName("databasename");

                entity.Property(e => e.Databasepassword)
                    .IsRequired()
                    .HasColumnName("databasepassword");

                entity.Property(e => e.Databaseserver)
                    .IsRequired()
                    .HasColumnName("databaseserver");

                entity.Property(e => e.Databaseusername)
                    .IsRequired()
                    .HasColumnName("databaseusername");

                entity.Property(e => e.Installationdate)
                    .IsRequired()
                    .HasColumnName("installationdate");

                entity.Property(e => e.Location).IsRequired();

                entity.Property(e => e.Modifydate)
                    .HasColumnType("datetime")
                    .HasColumnName("modifydate");

                entity.Property(e => e.Modifyuserid).HasColumnName("modifyuserid");

                entity.Property(e => e.Recordstatus)
                    .IsRequired()
                    .HasColumnName("recordstatus");

                entity.Property(e => e.SerialNumber).IsRequired();

                entity.Property(e => e.Sshipaddress)
                    .IsRequired()
                    .HasColumnName("sshipaddress");

                entity.Property(e => e.Sshport)
                    .IsRequired()
                    .HasColumnName("sshport");

                entity.Property(e => e.Sshusername)
                    .IsRequired()
                    .HasColumnName("sshusername");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasColumnName("version");

                entity.Property(e => e.YearOfManufac).IsRequired();
            });

            modelBuilder.Entity<TblOpMachinelog>(entity =>
            {
                entity.ToTable("tbl_op_machinelog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.EventLog).IsRequired();

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<TblOpMachinesetting>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_op_machinesetting");

                entity.HasIndex(e => e.Id, "ID");

                entity.Property(e => e.AirPressure).HasColumnType("decimal(20, 6)");

                entity.Property(e => e.CreateDate).HasPrecision(0);

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Estop)
                    .HasColumnType("decimal(20, 6)")
                    .HasColumnName("EStop");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LowPressure).HasColumnType("decimal(20, 6)");

                entity.Property(e => e.ModifyDate).HasPrecision(0);

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Temperature).HasColumnType("decimal(20, 6)");

                entity.Property(e => e._12vrange)
                    .HasColumnType("decimal(20, 6)")
                    .HasColumnName("12VRange");

                entity.Property(e => e._24vrange)
                    .HasColumnType("decimal(20, 6)")
                    .HasColumnName("24VRange");
            });

            modelBuilder.Entity<TblOpMachinestatus>(entity =>
            {
                entity.ToTable("tbl_op_machinestatus");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AirPressure)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CollectDateTime).HasColumnType("datetime");

                entity.Property(e => e.Estop)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("EStop");

                entity.Property(e => e.HourRun)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LowPressure)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Tempreature)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e._12ammeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("12Ammeter");

                entity.Property(e => e._12voltmeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("12Voltmeter");

                entity.Property(e => e._24ammeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("24Ammeter");

                entity.Property(e => e._24voltmeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("24Voltmeter");
            });

            modelBuilder.Entity<TblOpProduction>(entity =>
            {
                entity.ToTable("tbl_op_production");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BatchNo)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasPrecision(0);

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Inspector)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.ModifyDate).HasPrecision(0);

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Operator1)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_1");

                entity.Property(e => e.Operator2)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_2");

                entity.Property(e => e.Operator3)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_3");

                entity.Property(e => e.Operator4)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_4");

                entity.Property(e => e.ProductionSize)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ProductionType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblOpProductiondetail>(entity =>
            {
                entity.ToTable("tbl_op_productiondetail");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BatchNo).IsRequired();

                entity.Property(e => e.CollectDateTime).HasPrecision(0);

                entity.Property(e => e.Efficiency).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EndDateTime)
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("");

                entity.Property(e => e.Failed).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Interval).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NoGlove).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Passed).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Reject).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Retest).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.RoundTime).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StartDateTime)
                    .HasDefaultValueSql("(getdate())")
                    .HasComment("");

                entity.Property(e => e.Total).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<TblOpProductsetting>(entity =>
            {
                entity.ToTable("tbl_op_productsetting");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AirEjectGood).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AirEjectRetest).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.GloveType).IsRequired();

                entity.Property(e => e.Inflate1).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Inflate2).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InflatePressure).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.PassCode).IsRequired();

                entity.Property(e => e.ProductSize).IsRequired();

                entity.Property(e => e.ProductTitle).IsRequired();

                entity.Property(e => e.ProductType).IsRequired();

                entity.Property(e => e.RecordStatus).IsRequired();

                entity.Property(e => e.StartTest).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TestFor).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<TblOpReport>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_op_reports");

                entity.HasIndex(e => e.Id, "ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate)
                    .HasPrecision(0)
                    .HasDefaultValueSql("('0000-00-00 00:00:00')");

                entity.Property(e => e.CreateUserId)
                    .HasColumnName("CreateUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.Discount)
                    .HasColumnType("decimal(20, 6)")
                    .HasDefaultValueSql("('0.000000')");

                entity.Property(e => e.FromDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("('0000-00-00')");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Machines)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate)
                    .HasPrecision(0)
                    .HasDefaultValueSql("('0000-00-00 00:00:00')");

                entity.Property(e => e.ModifyUserId)
                    .HasColumnName("ModifyUserID")
                    .HasDefaultValueSql("('0')");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ReportId)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("ReportID");

                entity.Property(e => e.ToDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("('0000-00-00')");

                entity.Property(e => e.Year)
                    .HasColumnType("numeric(4, 0)")
                    .HasDefaultValueSql("('0000')");
            });

            modelBuilder.Entity<TblOpRfid>(entity =>
            {
                entity.ToTable("tbl_op_rfid");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CardId)
                    .IsRequired()
                    .HasColumnName("CardID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.Oplevel).HasColumnName("OPLevel");

                entity.Property(e => e.RecordStatus).IsRequired();

                entity.Property(e => e.Remarks).IsRequired();
            });

            modelBuilder.Entity<TblOpVersion>(entity =>
            {
                entity.ToTable("tbl_op_version");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.FileNameAliases)
                    .IsRequired()
                    .HasColumnName("FileName_aliases");

                entity.Property(e => e.FileNameExe)
                    .IsRequired()
                    .HasColumnName("FileName_Exe");

                entity.Property(e => e.FileNameIni)
                    .IsRequired()
                    .HasColumnName("FileName_ini");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.Note).IsRequired();

                entity.Property(e => e.RecordStatus).IsRequired();

                entity.Property(e => e.VersionName).IsRequired();
            });

            modelBuilder.Entity<Temp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("temp");

                entity.Property(e => e.AirPressure)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CollectDateTime).HasPrecision(0);

                entity.Property(e => e.Estop)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("EStop");

                entity.Property(e => e.HourRun)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.LowPressure)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Tempreature)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e._12ammeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("12Ammeter");

                entity.Property(e => e._12voltmeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("12Voltmeter");

                entity.Property(e => e._24ammeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("24Ammeter");

                entity.Property(e => e._24voltmeter)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("24Voltmeter");
            });

            modelBuilder.Entity<ViewCmUser>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_cm_user");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreateUserId).HasColumnName("CreateUserID");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastLoginDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.ModifyUserId).HasColumnName("ModifyUserID");

                entity.Property(e => e.Password).IsRequired();

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.UserRoleText)
                    .HasMaxLength(17)
                    .IsUnicode(false);

                entity.Property(e => e.Username).IsRequired();
            });

            modelBuilder.Entity<ViewDaFailed>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_Failed");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<ViewDaMachineStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_DA_MachineStatus");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");
            });

            modelBuilder.Entity<ViewDaMeterrunning>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_da_meterrunning");

                entity.Property(e => e.AirPressure).HasColumnType("decimal(38, 6)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Hourly).HasColumnType("time(0)");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.Tempreature).HasColumnType("decimal(38, 6)");

                entity.Property(e => e._12amm)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("12Amm");

                entity.Property(e => e._12volt)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("12Volt");

                entity.Property(e => e._24amm)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("24Amm");

                entity.Property(e => e._24volt)
                    .HasColumnType("decimal(38, 6)")
                    .HasColumnName("24Volt");
            });

            modelBuilder.Entity<ViewDaNoglove>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_noglove");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<ViewDaPassed>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_Passed");

                entity.Property(e => e.Machineid).HasColumnName("machineid");
            });

            modelBuilder.Entity<ViewDaReject>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_reject");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<ViewDaRetest>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_retest");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<ViewDaTotal>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_DA_Total");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");
            });

            modelBuilder.Entity<ViewDaYieldcompany>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_da_yieldcompany");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e._0)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("0");

                entity.Property(e => e._1)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("1");

                entity.Property(e => e._10)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("10");

                entity.Property(e => e._11)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("11");

                entity.Property(e => e._12)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("12");

                entity.Property(e => e._2)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("2");

                entity.Property(e => e._3)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("3");

                entity.Property(e => e._4)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("4");

                entity.Property(e => e._5)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("5");

                entity.Property(e => e._6)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("6");

                entity.Property(e => e._7)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("7");

                entity.Property(e => e._8)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("8");

                entity.Property(e => e._9)
                    .HasColumnType("decimal(38, 0)")
                    .HasColumnName("9");
            });

            modelBuilder.Entity<ViewOpBatchanalysis>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_op_batchanalysis");

                entity.Property(e => e.BatchNo).IsRequired();

                entity.Property(e => e.Efficiency).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Failed).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Interval).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.NoGlove).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Passed).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Rate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Retest).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.RoundTime).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.Total).HasColumnType("decimal(38, 0)");
            });

            modelBuilder.Entity<ViewOpBatchlist>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_op_batchlist");

                entity.Property(e => e.BatchNo).IsRequired();

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.Operator1)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_1");

                entity.Property(e => e.Operator2)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_2");

                entity.Property(e => e.Operator3)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_3");

                entity.Property(e => e.Operator4)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_4");

                entity.Property(e => e.ProductionSize)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ProductionType)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViewOpMachineviewcompany>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_op_machineviewcompany");

                entity.Property(e => e.Calibrationdate)
                    .IsRequired()
                    .HasColumnName("calibrationdate");

                entity.Property(e => e.CompanyAddress).IsRequired();

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.CompanyName).IsRequired();

                entity.Property(e => e.Createdate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdate");

                entity.Property(e => e.Createuserid).HasColumnName("createuserid");

                entity.Property(e => e.Databasename)
                    .IsRequired()
                    .HasColumnName("databasename");

                entity.Property(e => e.Databasepassword)
                    .IsRequired()
                    .HasColumnName("databasepassword");

                entity.Property(e => e.Databaseserver)
                    .IsRequired()
                    .HasColumnName("databaseserver");

                entity.Property(e => e.Databaseusername)
                    .IsRequired()
                    .HasColumnName("databaseusername");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Installationdate)
                    .IsRequired()
                    .HasColumnName("installationdate");

                entity.Property(e => e.Location).IsRequired();

                entity.Property(e => e.Modifydate)
                    .HasColumnType("datetime")
                    .HasColumnName("modifydate");

                entity.Property(e => e.Modifyuserid).HasColumnName("modifyuserid");

                entity.Property(e => e.Recordstatus)
                    .IsRequired()
                    .HasColumnName("recordstatus");

                entity.Property(e => e.SerialNumber).IsRequired();

                entity.Property(e => e.Sshipaddress)
                    .IsRequired()
                    .HasColumnName("sshipaddress");

                entity.Property(e => e.Sshport)
                    .IsRequired()
                    .HasColumnName("sshport");

                entity.Property(e => e.Sshusername)
                    .IsRequired()
                    .HasColumnName("sshusername");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasColumnName("version");

                entity.Property(e => e.YearOfManufac).IsRequired();
            });

            modelBuilder.Entity<ViewOpProductioninfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_op_productioninfo");

                entity.Property(e => e.BatchNo)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasPrecision(0);

                entity.Property(e => e.Inspector)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.ModifyDate).HasPrecision(0);

                entity.Property(e => e.Operator1)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_1");

                entity.Property(e => e.Operator2)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_2");

                entity.Property(e => e.Operator3)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_3");

                entity.Property(e => e.Operator4)
                    .IsRequired()
                    .IsUnicode(false)
                    .HasColumnName("Operator_4");

                entity.Property(e => e.ProductionSize)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ProductionType)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViewOpRfidinfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("view_op_RFIDinfo");

                entity.Property(e => e.Cardid)
                    .IsRequired()
                    .HasColumnName("cardid");

                entity.Property(e => e.Companyid).HasColumnName("companyid");

                entity.Property(e => e.Createdate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdate");

                entity.Property(e => e.Createuserid).HasColumnName("createuserid");

                entity.Property(e => e.Employeeid).HasColumnName("employeeid");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Modifydate)
                    .HasColumnType("datetime")
                    .HasColumnName("modifydate");

                entity.Property(e => e.Modifyuserid).HasColumnName("modifyuserid");

                entity.Property(e => e.Oplevel)
                    .HasMaxLength(16)
                    .IsUnicode(false)
                    .HasColumnName("oplevel");

                entity.Property(e => e.OplevelId).HasColumnName("OPlevelID");

                entity.Property(e => e.Recordstatus)
                    .IsRequired()
                    .HasColumnName("recordstatus");

                entity.Property(e => e.Remarks)
                    .IsRequired()
                    .HasColumnName("remarks");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
