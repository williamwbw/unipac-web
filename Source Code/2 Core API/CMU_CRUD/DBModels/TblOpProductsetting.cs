﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpProductsetting
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string ProductTitle { get; set; }
        public string ProductType { get; set; }
        public string ProductSize { get; set; }
        public decimal InflatePressure { get; set; }
        public decimal Inflate1 { get; set; }
        public decimal Inflate2 { get; set; }
        public int RejectAssist { get; set; }
        public int GloveIllumination { get; set; }
        public decimal AirEjectGood { get; set; }
        public decimal AirEjectRetest { get; set; }
        public decimal StartTest { get; set; }
        public decimal TestFor { get; set; }
        public string PassCode { get; set; }
        public string GloveType { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
