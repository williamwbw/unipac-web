﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewOpRfidinfo
    {
        public long? No { get; set; }
        public int Id { get; set; }
        public string Cardid { get; set; }
        public int Companyid { get; set; }
        public int OplevelId { get; set; }
        public string Oplevel { get; set; }
        public string Employee { get; set; }
        public string Employeeid { get; set; }
        public string Remarks { get; set; }
        public string Recordstatus { get; set; }
        public int Createuserid { get; set; }
        public DateTime Createdate { get; set; }
        public int Modifyuserid { get; set; }
        public DateTime Modifydate { get; set; }
    }
}
