﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpVersion
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string VersionName { get; set; }
        public int VersionGeneration { get; set; }
        public string FileNameExe { get; set; }
        public string FileNameAliases { get; set; }
        public string FileNameIni { get; set; }
        public string Note { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
