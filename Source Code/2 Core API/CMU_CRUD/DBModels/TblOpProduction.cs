﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpProduction
    {
        public int Id { get; set; }
        public int MachineId { get; set; }
        public string ProductionType { get; set; }
        public string ProductionSize { get; set; }
        public string BatchNo { get; set; }
        public string Operator1 { get; set; }
        public string Operator2 { get; set; }
        public string Operator3 { get; set; }
        public string Operator4 { get; set; }
        public string Inspector { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
