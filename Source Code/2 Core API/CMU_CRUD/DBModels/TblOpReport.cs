﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpReport
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string ReportId { get; set; }
        public string Remarks { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Year { get; set; }
        public string Machines { get; set; }
        public decimal Discount { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
