﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewOpBatchlist
    {
        public string BatchNo { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int MachineId { get; set; }
        public string ProductionType { get; set; }
        public string Operator1 { get; set; }
        public string Operator2 { get; set; }
        public string Operator3 { get; set; }
        public string Operator4 { get; set; }
        public string ProductionSize { get; set; }
    }
}
