﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class ViewOpBatchanalysis
    {
        public string BatchNo { get; set; }
        public decimal? Passed { get; set; }
        public decimal? Failed { get; set; }
        public decimal? Retest { get; set; }
        public decimal? Total { get; set; }
        public decimal? NoGlove { get; set; }
        public decimal? Rate { get; set; }
        public decimal? RoundTime { get; set; }
        public decimal? Interval { get; set; }
        public decimal? Efficiency { get; set; }
    }
}
