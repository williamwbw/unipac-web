﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CMU_CRUD.DBModels
{
    public partial class TblOpRfid
    {
        public int Id { get; set; }
        public string CardId { get; set; }
        public int CompanyId { get; set; }
        public int Oplevel { get; set; }
        public int ManageByUser { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Remarks { get; set; }
        public string RecordStatus { get; set; }
        public int CreateUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
