﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class DA
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Years { get; set; }
        public int Pass { get; set; }
        public int Retest { get; set; }
        public int Failed { get; set; }
        public int Reject { get; set; }
        public int NoGlove { get; set; }
        public int Total { get; set; }

        public DA()
        {

        }
    }
}
