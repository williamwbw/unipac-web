﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class EventLog
    {
		public DateTime cDateTime { get; set; }
		public string cEvent { get; set; }

		public EventLog()
        {

        }

		public EventLog(DateTime cDateTime_, string cEvent_)
		{
			this.cDateTime = cDateTime_;
			this.cEvent = cEvent_;
		}
	}
}
