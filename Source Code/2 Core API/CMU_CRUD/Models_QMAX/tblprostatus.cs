﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class tblprostatus
    {
		public int cId { get; set; }
		public string cProductType { get; set; }
		public string cBatchNo { get; set; }
		public string cProductSize { get; set; }
		public string cOperator1 { get; set; }
		public string cOperator2 { get; set; }
		public string cInspector { get; set; }
		public string cRunTime { get; set; }
		public int cModule { get; set; }
		public int cPassed { get; set; }
		public int cFailed { get; set; }
		public int cRetest { get; set; }
		public int cReject { get; set; }
		public int cTotal { get; set; }
		public int cNoGlove { get; set; }
		public int cRate { get; set; }
		public double cRoundTime { get; set; }
		public double cInterval { get; set; }
		public double cEfficiency { get; set; }

		public double cPesPassed { get; set; }
		public double cPesFailed { get; set; }
		public double cPesRetest { get; set; }
		public double cPesReject { get; set; }
		public double cPesNoGlove { get; set; }

		public tblprostatus()
        {

        }

		public tblprostatus(int cId_, string cProductType_, string cBatchNo_, string cProductSize_, string cOperator1_, string cOperator2_, string cInspector_, string cRunTime_, int cModule_, int cPassed_, int cFailed_, int cRetest_, int cReject_, int cTotal_, int cNoGlove_, int cRate_, double cRoundTime_, double cInterval_, double cEfficiency_)
		{
			this.cId = cId_;
			this.cProductType = cProductType_;
			this.cBatchNo = cBatchNo_;
			this.cProductSize = cProductSize_;
			this.cOperator1 = cOperator1_;
			this.cOperator2 = cOperator2_;
			this.cInspector = cInspector_;
			this.cRunTime = cRunTime_;
			this.cModule = cModule_;
			this.cPassed = cPassed_;
			this.cFailed = cFailed_;
			this.cRetest = cRetest_;
			this.cReject = cReject_;
			this.cTotal = cTotal_;
			this.cNoGlove = cNoGlove_;
			this.cRate = cRate_;
			this.cRoundTime = cRoundTime_;
			this.cInterval = cInterval_;
			this.cEfficiency = cEfficiency_;
		}
	}
}
