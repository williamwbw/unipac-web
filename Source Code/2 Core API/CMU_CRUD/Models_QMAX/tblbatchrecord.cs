﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class tblbatchrecord
    {
		public DateTime cStarttime { get; set; }
		public DateTime cEndtime { get; set; }
		public string cMachineNo { get; set; }
		public string cProductType { get; set; }
		public string cBatchNo { get; set; }
		public string cProductSize { get; set; }
		public double cRate { get; set; }
		public string cGloveType { get; set; }
		public string cOperator1_Name { get; set; }
		public string cOperator1_ID { get; set; }
		public int cPassed_Op { get; set; }
		public int cRetest_Op { get; set; }
		public int cFailed_Op { get; set; }
		public int cReject_Op { get; set; }
		public int cNoGlove_Op { get; set; }
		public int cTotal { get; set; }
		public string cOperator2_Name { get; set; }
		public string cOperator2_ID { get; set; }
		public string cInspector_Name { get; set; }
		public string cInspector_ID { get; set; }

		public tblbatchrecord()
        {

        }
	}
}
