﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class DABatch
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Years { get; set; }
        public int Pass { get; set; }
        public int Retest { get; set; }
        public int Failed { get; set; }
        public int Reject { get; set; }
        public int NoGlove { get; set; }
        public int Total { get; set; }
        public string cMachineNo { get; set; }
        public string cRate { get; set; }
        public string cOperator1_Name { get; set; }
        public string cOperator2_Name { get; set; }
        public string cInspector_Name { get; set; }
        public string cBatchNo { get; set; }

        public DABatch()
        {

        }
    }
}
