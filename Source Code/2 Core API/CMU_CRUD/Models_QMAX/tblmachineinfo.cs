﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Models_QMAX
{
    public class tblmachineinfo
    {
		public int cId { get; set; }
		public string cInfoName { get; set; }
		public string cInfo { get; set; }

		public tblmachineinfo(int cId_, string cInfoName_, string cInfo_)
		{
			this.cId = cId_;
			this.cInfoName = cInfoName_;
			this.cInfo = cInfo_;
		}
	}
}
