﻿using CMU_CRUD.DBModels;
using CMU_CRUD.Lib;
using CMU_CRUD.Models_QMAX;
//using CMU_CRUD.DBModels;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatabaseSync : ControllerBase
    {
        unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
        // GET: api/<DatabaseSync>
        //Collect Database from all the QMAX machine
        [HttpGet]
        public async Task<IEnumerable<string>> GetAsync()
        {
            var localcmu = unipac_CmuContext.TblOpLocalCmus.Where(x => x.RecordStatus != "Inactive").ToList();
            foreach (var CMU in localcmu)
            {
                WriteToFile(DateTime.Now + ": Connection Started");
                var sshServer = CMU.Sshipaddress;
                var sshUserName = CMU.SshuserName;
                var sshPassword = "";
                var databaseServer = CMU.Dbserver;
                var databaseUserName = CMU.DbuserName;
                var databasePassword = CMU.Dbpassword;
                var sshPort = Convert.ToInt32(CMU.Sshport);
                var sshKeyFile = "id_rsa";
                var database = CMU.Dbname;
                WriteToFile(DateTime.Now + ": Local CMU Infor : " + sshServer + ", " + sshUserName);
                WriteToFile(DateTime.Now + ": Local CMU Infor DB : " + database);
                var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

                try
                {
                    WriteToFile(DateTime.Now + ": local CMU SSH : " + sshClient);
                    using (sshClient)
                    {
                        MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                        {
                            Server = "127.0.0.1",
                            Port = localPort,
                            UserID = databaseUserName,
                            Password = databasePassword,
                            Database = database
                        };
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        WriteToFile(DateTime.Now + ": Local CMU Connection : " + connection.ConnectionString  );
                        await connection.OpenAsync();
                        MySqlDataReader reader;
                        string query = "SELECT * FROM tblmachinestatus WHERE cMachineID <> ''";
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        reader = cmd.ExecuteReader();
                        WriteToFile(DateTime.Now + ": Local CMU Machine Reading Done...");
                        while (reader.Read())
                        {
                            WriteToFile(DateTime.Now + ": Local CMU Machine Reading Data...");
                            try
                            {
                                WriteToFile(DateTime.Now + ": Local CMU Machine Reading Detail Data...");
                                string MachineID = reader.GetString("cMachineID");
                                string MachineStatus = reader.GetString("cStatus");
                                var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Recordstatus != "Inactive" && x.SerialNumber == MachineID).FirstOrDefault();
                                WriteToFile(DateTime.Now + ": Cloud CMU Machine Infor : " + machineslist);
                                if (machineslist != null)
                                {
                                    WriteToFile(DateTime.Now + ": Machine Infor : " + MachineID);
                                    WriteToFile(DateTime.Now + ": Machine Infor Status : " + MachineStatus);
                                    switch (MachineStatus)
                                    {
                                        case "STANDBY":
                                            machineslist.Recordstatus = "Standby";
                                            break;
                                        case "PRODUCTION":
                                            machineslist.Recordstatus = "Production";
                                            break;
                                        case "OFF":
                                            machineslist.Recordstatus = "Off";
                                            break;
                                    }
                                }
                                unipac_CmuContext.SaveChanges();
                                //if(MachineStatus != " OFF") { MachineInfo(machineslist); }
                                
                            }
                            catch(Exception ex)
                            {
                                WriteToFile(DateTime.Now + ": Connection Reader : " + ex.InnerException.Message);
                            }
                        }
                        reader.Close();
                    }
                }catch(Exception ex)
                {
                    WriteToFile(DateTime.Now + ": Connection Issue : " + ex.Message);
                }
            }


            //Machine Sensor Update
            var machinelist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Recordstatus != "Inactive" && x.Recordstatus != "Off").ToList();
            foreach (var machine in machinelist) {
                MachineInfo(machine);
            }

            return null;
        }

        public (SshClient SshClient, uint Port) ConnectSsh(string sshHostName, string sshUserName, string sshPassword = null,
string sshKeyFile = "C:\\CMU\\API\\CRUD\\id_rsa", string sshPassPhrase = null, int sshPort = 5002, string databaseServer = "localhost", int databasePort = 3306)
        {
            WriteToFile(DateTime.Now + ": SSHClient Processing...");
            // check arguments
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshHostName)} must be specified.", nameof(sshHostName));
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshUserName)} must be specified.", nameof(sshUserName));
            if (string.IsNullOrEmpty(sshPassword) && string.IsNullOrEmpty(sshKeyFile))
                throw new ArgumentException($"One of {nameof(sshPassword)} and {nameof(sshKeyFile)} must be specified.");
            if (string.IsNullOrEmpty(databaseServer))
                throw new ArgumentException($"{nameof(databaseServer)} must be specified.", nameof(databaseServer));

            // define the authentication methods to use (in order)
            var authenticationMethods = new List<AuthenticationMethod>();
            try
            {
                WriteToFile(DateTime.Now + ": SSHClient Processing...KeyFile: " + sshKeyFile);
                if (!string.IsNullOrEmpty(sshKeyFile))
                {
                    WriteToFile(DateTime.Now + ": SSHClient Processing...sshUserName: " + sshUserName);
                    WriteToFile(DateTime.Now + ": SSHClient Processing...sshPassPhrase: " + sshPassPhrase);
                    authenticationMethods.Add(new PrivateKeyAuthenticationMethod(sshUserName,
                        new PrivateKeyFile(sshKeyFile, string.IsNullOrEmpty(sshPassPhrase) ? null : sshPassPhrase)));
                    WriteToFile(DateTime.Now + ": SSHClient Processing...Done: " + authenticationMethods);
                }
            }
            catch (Exception ex)
            {
                WriteToFile(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
            }
            try
            {
                if (!string.IsNullOrEmpty(sshPassword))
                {
                    authenticationMethods.Add(new PasswordAuthenticationMethod(sshUserName, sshPassword));
                }
            }catch(Exception ex)
            {
                WriteToFile(DateTime.Now + ": SSHClient Processing...Error Password: " + ex.InnerException.Message);
            }

            // connect to the SSH server
            var sshClient = new SshClient(new ConnectionInfo(sshHostName, sshPort, sshUserName, authenticationMethods.ToArray()));
            sshClient.Connect();
            // forward a local port to the database server and port, using the SSH server
            var forwardedPort = new Renci.SshNet.ForwardedPortLocal("127.0.0.1", databaseServer, (uint)databasePort);
            sshClient.AddForwardedPort(forwardedPort);
            forwardedPort.Start();

            return (sshClient, forwardedPort.BoundPort);
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\DatabaseSyncLogs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\DatabaseSyncLogs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        public void MachineInfo(TblOpMachineinfo Machine)
        {
            WriteToFile(" Data Sync Starting.....");
            try
            {
                MySqlDataReader reader;

                var sshServer = Machine.Sshipaddress;
                var sshUserName = Machine.Sshusername;
                var sshPassword = "";
                var databaseServer = Machine.Databaseserver;
                var databaseUserName = Machine.Databaseusername;
                var databasePassword = Machine.Databasepassword;
                var sshPort = Convert.ToInt32(Machine.Sshport);
                var sshKeyFile = "id_rsa";
                var database = Machine.Databasename;
                var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database
                    };
                    using var Conn = new MySqlConnection(csb.ConnectionString);
                    Conn.Open();

                    try
                    {
                        TblOpMachineinfo objMachine = new TblOpMachineinfo();

                        var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == Machine.Id);
                        var MachineStatus = unipac_CmuContext.TblOpMachinestatuses.Where(x => x.MachineId == Machine.Id).OrderByDescending(x => x.CollectDateTime).FirstOrDefault();
                        if (MachineList.Count() > 0)
                        {
                            //Create Mysql Command
                            string query = "SELECT * FROM tblmachineinfo";
                            MySqlCommand cmd = new MySqlCommand(query, Conn);
                            reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                switch (reader.GetString("cInfoName"))
                                {
                                    case "MACHINE ID":
                                        WriteToFile(reader.GetString("cInfo") + " Data Sync Start");
                                        MachineList.First<TblOpMachineinfo>().SerialNumber = reader.GetString("cInfo");
                                        break;
                                    case "CALIBRATION DUE DATE":
                                        MachineList.First<TblOpMachineinfo>().Calibrationdate = reader.GetString("cInfo");
                                        break;
                                    case "DATE OF INSTALLATION":
                                        MachineList.First<TblOpMachineinfo>().Installationdate = reader.GetString("cInfo");
                                        break;
                                    case "SOFTWARE VERSION":
                                        MachineList.First<TblOpMachineinfo>().Version = reader.GetString("cInfo");
                                        break;
                                }
                            }
                            WriteToFile(MachineList.First<TblOpMachineinfo>().SerialNumber + " Data Sync info done");
                            reader.Close();

                            query = "SELECT * FROM tblmachinestatus";
                            cmd = new MySqlCommand(query, Conn);
                            reader = cmd.ExecuteReader();
                            WriteToFile(MachineList.First<TblOpMachineinfo>().SerialNumber + " Machine Status Data Sync info Start");
                            while (reader.Read())
                            {
                                if (MachineStatus != null)
                                {
                                    WriteToFile("Machine Previous Status: updating....");
                                    MachineStatus.RecordStatus = "Pass";
                                    WriteToFile("Machine Previous Status: Done....");
                                }

                                TblOpMachinestatus objMachineStatus = new TblOpMachinestatus();
                                objMachineStatus.RecordStatus = "Pass";
                                objMachineStatus.CollectDateTime = DateTime.Now;
                                objMachineStatus.MachineId = Machine.Id;
                                objMachineStatus._12voltmeter = reader.GetString("c12V_V");
                                objMachineStatus._24voltmeter = reader.GetString("c24V_V");
                                objMachineStatus._12ammeter = reader.GetString("c12V_A");
                                objMachineStatus._24ammeter = reader.GetString("c24V_A");
                                objMachineStatus.AirPressure = reader.GetString("cPressure");
                                objMachineStatus.Tempreature = reader.GetString("cTemperature");
                                objMachineStatus.LowPressure = reader.GetString("cLowPressure");
                                objMachineStatus.Estop = reader.GetString("cESTOP");
                                objMachineStatus.HourRun = reader.GetString("cHourRun");
                                unipac_CmuContext.TblOpMachinestatuses.Add(objMachineStatus);
                                unipac_CmuContext.SaveChanges();


                                if (Convert.ToDouble(reader.GetString("c12V_V")) >= 22.8 && Convert.ToDouble(reader.GetString("c12V_V")) <= 25.2)
                                {
                                    var LogFault = unipac_CmuContext.TblOpMachinefaultlogs.Where(x => x.MachineId == Machine.Id && x.Event == "12 Voltmeter").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                                    if (DateTime.Now.Subtract(LogFault.CreateDate).TotalMinutes >= 10 || LogFault != null)
                                    {
                                        TblOpMachinefaultlog objfaultlog = new TblOpMachinefaultlog();
                                        objfaultlog.MachineId = Machine.Id;
                                        objfaultlog.Event = "12 Voltmeter";
                                        objfaultlog.FaultLog = "Volt in between 22.8 ~ 25.2";
                                        objfaultlog.CreateDate = DateTime.Now;
                                        objfaultlog.CreateUserId = 1;
                                        unipac_CmuContext.TblOpMachinefaultlogs.Add(objfaultlog);
                                    }
                                }

                                if (reader.GetInt32("cLowPressure") > 0)
                                {
                                    var LogFault = unipac_CmuContext.TblOpMachinefaultlogs.Where(x => x.MachineId == Machine.Id && x.Event == "Low Pressure").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                                    if (DateTime.Now.Subtract(LogFault.CreateDate).TotalMinutes >= 10 || LogFault != null)
                                    {
                                        TblOpMachinefaultlog objfaultlog = new TblOpMachinefaultlog();
                                        objfaultlog.MachineId = Machine.Id;
                                        objfaultlog.Event = "Low Pressure";
                                        objfaultlog.FaultLog = "Low Pressure Detected";
                                        objfaultlog.CreateDate = DateTime.Now;
                                        objfaultlog.CreateUserId = 1;
                                        unipac_CmuContext.TblOpMachinefaultlogs.Add(objfaultlog);
                                    }
                                }

                                if (reader.GetInt32("cPressure") == 0)
                                {
                                    var LogFault = unipac_CmuContext.TblOpMachinefaultlogs.Where(x => x.MachineId == Machine.Id && x.Event == "Air Pressure").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                                    if (DateTime.Now.Subtract(LogFault.CreateDate).TotalMinutes >= 10 || LogFault != null)
                                    {
                                        TblOpMachinefaultlog objfaultlog = new TblOpMachinefaultlog();
                                        objfaultlog.MachineId = Machine.Id;
                                        objfaultlog.Event = "Air Pressure";
                                        objfaultlog.FaultLog = "Air Pressure 0psi Detected";
                                        objfaultlog.CreateDate = DateTime.Now;
                                        objfaultlog.CreateUserId = 1;
                                        unipac_CmuContext.TblOpMachinefaultlogs.Add(objfaultlog);
                                    }
                                }

                                if (reader.GetInt32("cTemperature") == 0)
                                {
                                    var LogFault = unipac_CmuContext.TblOpMachinefaultlogs.Where(x => x.MachineId == Machine.Id && x.Event == "Temperature").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                                    if (DateTime.Now.Subtract(LogFault.CreateDate).TotalMinutes >= 10 || LogFault != null)
                                    {
                                        TblOpMachinefaultlog objfaultlog = new TblOpMachinefaultlog();
                                        objfaultlog.MachineId = Machine.Id;
                                        objfaultlog.Event = "Temperature";
                                        objfaultlog.FaultLog = "Temperature 0'C Detected";
                                        objfaultlog.CreateDate = DateTime.Now;
                                        objfaultlog.CreateUserId = 1;
                                        unipac_CmuContext.TblOpMachinefaultlogs.Add(objfaultlog);
                                    }
                                }
                            }
                            reader.Close();
                            MachineList.First<TblOpMachineinfo>().Modifydate = DateTime.Now;
                            unipac_CmuContext.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteToFile("Error: " + ex.InnerException.Message);
                    }
                }
            }catch(Exception ex)
            {
                WriteToFile("Error Status: " + ex.InnerException.Message);
            }
        }
    }
}
