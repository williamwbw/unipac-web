﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using CMU_CRUD.Lib;
using Microsoft.AspNetCore.Http;
using System.IO;
using MySql.Data.MySqlClient;
using CMU_CRUD.Controllers;
using CMU_CRUD.Models_QMAX;
using System.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MachineInfoController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();
        // GET: api/<MachineInfoController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Information
        [HttpGet("{id}")]
        public string Get(int id)
        {
            TblOpMachineinfo objMachine = new TblOpMachineinfo();
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineList = unipac_CmuContext.ViewOpMachineviewcompanies.ToList().Where(x => x.Id == id).FirstOrDefault();
            string json = JsonConvert.SerializeObject(machineList);
            return json;
        }

        [HttpGet("GetOccupancy/{id}")]
        public string GetOccupancy(int id)
        {
            TblOpMachineinfo objMachine = new TblOpMachineinfo();
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineList = unipac_CmuContext.ViewDaMachineStatuses.ToList().Where(x => x.CompanyId == id).FirstOrDefault();
            string json = JsonConvert.SerializeObject(machineList);
            return json;
        }

        // GET api/<MachineInfoController>/5
        //Get All Machine With Company
        [HttpGet("GetMachineWCompany/{id}")]
        public string GetMachineWCompany(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.ToList().Where(x => x.CompanyId == id && x.Recordstatus != "Inactive");
            string json = JsonConvert.SerializeObject(machineslist, objCSetting.JsonFormatdate());
            return json;
        }

        // GET api/<MachineInfoController>/5
        //Get All Machine With Company - added sy
        [HttpGet("GetAllMachineWCompany")]
        public string GetAllMachineWCompany()
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.ViewOpMachineviewcompanies.ToList();
            string json = JsonConvert.SerializeObject(machineslist, objCSetting.JsonFormatdate());
            return json;
        }       

        public (Renci.SshNet.SshClient SshClient, uint Port) ConnectSsh(string sshHostName, string sshUserName, string sshPassword = null,
string sshKeyFile = "id_rsa", string sshPassPhrase = null, int sshPort = 5003, string databaseServer = "localhost", int databasePort = 3306)
        {
            //WriteToFile(DateTime.Now + ": SSHClient Processing...");
            // check arguments
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshHostName)} must be specified.", nameof(sshHostName));
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshUserName)} must be specified.", nameof(sshUserName));
            if (string.IsNullOrEmpty(sshPassword) && string.IsNullOrEmpty(sshKeyFile))
                throw new ArgumentException($"One of {nameof(sshPassword)} and {nameof(sshKeyFile)} must be specified.");
            if (string.IsNullOrEmpty(databaseServer))
                throw new ArgumentException($"{nameof(databaseServer)} must be specified.", nameof(databaseServer));

            // define the authentication methods to use (in order)
            var authenticationMethods = new List<Renci.SshNet.AuthenticationMethod>();
            try
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...KeyFile: " + sshKeyFile);
                if (!string.IsNullOrEmpty(sshKeyFile))
                {
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshUserName: " + sshUserName);
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshPassPhrase: " + sshPassPhrase);
                    authenticationMethods.Add(new Renci.SshNet.PrivateKeyAuthenticationMethod(sshUserName,
                        new Renci.SshNet.PrivateKeyFile(sshKeyFile, string.IsNullOrEmpty(sshPassPhrase) ? null : sshPassPhrase)));
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...Done: " + authenticationMethods);
                }
            }
            catch (Exception ex)
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
            }
            try
            {
                if (!string.IsNullOrEmpty(sshPassword))
                {
                    authenticationMethods.Add(new Renci.SshNet.PasswordAuthenticationMethod(sshUserName, sshPassword));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Password: " + ex.InnerException.Message);
            }

            // connect to the SSH server
            var sshClient = new Renci.SshNet.SshClient(new Renci.SshNet.ConnectionInfo(sshHostName, sshPort, sshUserName, authenticationMethods.ToArray()));
            sshClient.Connect();

            // forward a local port to the database server and port, using the SSH server
            var forwardedPort = new Renci.SshNet.ForwardedPortLocal("127.0.0.1", databaseServer, (uint)databasePort);
            sshClient.AddForwardedPort(forwardedPort);
            forwardedPort.Start();

            return (sshClient, forwardedPort.BoundPort);
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Status
        [HttpGet("getStatus/{id}")]
        public async Task<string> GetStatusAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    using var connection = new MySqlConnection(csb.ConnectionString);
                    await connection.OpenAsync();
                    string Query = string.Format(@"SELECT * FROM tblmachinestatus");
                    MySqlCommand cmd = new MySqlCommand(Query, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    TblOpMachinestatus objMachineStatus = new TblOpMachinestatus();
                    while (await reader.ReadAsync())
                    {
                        objMachineStatus._12voltmeter = reader.GetString("c12V_V");
                        objMachineStatus._24voltmeter = reader.GetString("c24V_V");
                        objMachineStatus._12ammeter = reader.GetString("c12V_A");
                        objMachineStatus._24ammeter = reader.GetString("c24V_A");
                        objMachineStatus.AirPressure = reader.GetString("cPressure");
                        objMachineStatus.Tempreature = reader.GetString("cTemperature");
                        objMachineStatus.LowPressure = reader.GetString("cLowPressure");
                        objMachineStatus.Estop = reader.GetString("cESTOP");
                        objMachineStatus.HourRun = reader.GetString("cHourRun");
                        TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(objMachineStatus.HourRun));
                        objMachineStatus.HourRun = string.Format("{0}:{1:00}", (int)ts.TotalHours, ts.Minutes);
                        objMachineStatus.RecordStatus = reader.GetString("cMode");

                        //Change Value Format
                        objMachineStatus._12ammeter = Convert.ToDecimal(objMachineStatus._12ammeter).ToString("0.##");
                        objMachineStatus._24ammeter = Convert.ToDecimal(objMachineStatus._24ammeter).ToString("0.##");
                        objMachineStatus._24voltmeter = Convert.ToDecimal(objMachineStatus._24voltmeter).ToString("0.##");
                        objMachineStatus._12voltmeter = Convert.ToDecimal(objMachineStatus._12voltmeter).ToString("0.##");
                        objMachineStatus._12voltmeter = Convert.ToDecimal(objMachineStatus._12voltmeter).ToString("0.##");
                        objMachineStatus.AirPressure = Convert.ToDecimal(objMachineStatus.AirPressure).ToString("0.##");
                        objMachineStatus.Tempreature = Convert.ToDecimal(objMachineStatus.Tempreature).ToString("0.##");
                    }
                    connection.Close();

                    string json = JsonConvert.SerializeObject(objMachineStatus, objCSetting.JsonFormatdate());
                    return json;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return ex.Message;
            }

            //TblOpMachinestatus objMachineStatus = new TblOpMachinestatus();
            
            //var machinestatuslist = unipac_CmuContext.TblOpMachinestatuses.ToList().Where(x => x.MachineId == id && x.RecordStatus == "Active").OrderByDescending(x => x.CollectDateTime).FirstOrDefault();
            //objMachineStatus = machinestatuslist;
            //objMachineStatus._12ammeter = Convert.ToDecimal(objMachineStatus._12ammeter).ToString("0.##");
            //objMachineStatus._24ammeter = Convert.ToDecimal(objMachineStatus._24ammeter).ToString("0.##");
            //objMachineStatus._24voltmeter = Convert.ToDecimal(objMachineStatus._24voltmeter).ToString("0.##");
            //objMachineStatus._12voltmeter = Convert.ToDecimal(objMachineStatus._12voltmeter).ToString("0.##");
            //objMachineStatus._12voltmeter = Convert.ToDecimal(objMachineStatus._12voltmeter).ToString("0.##");
            //objMachineStatus.AirPressure = Convert.ToDecimal(objMachineStatus.AirPressure).ToString("0.##");
            //objMachineStatus.Tempreature = Convert.ToDecimal(objMachineStatus.Tempreature).ToString("0.##");
            //TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(objMachineStatus.HourRun));
            ////objMachineStatus.HourRun = ts.TotalHours.ToString("0");
            //objMachineStatus.HourRun = string.Format("{0}:{1:00}", (int)ts.TotalHours, ts.Minutes);
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Production
        [HttpGet("GetProduction/{id}")]
        public async Task<string> GetProductionAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    using var connection = new MySqlConnection(csb.ConnectionString);
                    await connection.OpenAsync();
                    string Query = string.Format(@"SELECT * FROM tblprodstatus");
                    MySqlCommand cmd = new MySqlCommand(Query, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    tblprostatus objprostatus = new tblprostatus();
                    while (await reader.ReadAsync())
                    {
                        objprostatus.cBatchNo = reader.GetString("cBatchNo");
                        objprostatus.cEfficiency = Convert.ToDouble(reader.GetDouble("cEfficiency").ToString("0.##"));
                        objprostatus.cFailed = reader.GetInt32("cFailed");
                        objprostatus.cInspector = reader.GetString("cInspector");
                        objprostatus.cInterval = Convert.ToDouble(reader.GetDouble("cInterval").ToString("0.##"));
                        objprostatus.cModule = reader.GetInt32("cModule");
                        objprostatus.cNoGlove = reader.GetInt32("cNoGlove");
                        objprostatus.cOperator1 = reader.GetString("cOperator1");
                        objprostatus.cOperator2 = reader.GetString("cOperator2");
                        objprostatus.cPassed = reader.GetInt32("cPassed");
                        objprostatus.cProductSize = reader.GetString("cProductSize");
                        objprostatus.cProductType = reader.GetString("cProductType");
                        objprostatus.cRate = reader.GetInt32("cRate");
                        objprostatus.cReject = reader.GetInt32("cReject");
                        objprostatus.cRetest = reader.GetInt32("cRetest");
                        objprostatus.cRoundTime = Convert.ToDouble(reader.GetDouble("cRoundTime").ToString("0.##"));
                        objprostatus.cRunTime = reader.GetString("cRunTime");
                        objprostatus.cTotal = reader.GetInt32("cTotal");

                        //calculation
                        objprostatus.cPesPassed = reader.GetDouble("cTotal") > 0 ? Convert.ToDouble((reader.GetDouble("cPassed") / reader.GetDouble("cTotal") * 100).ToString("0.##")) : 0;
                        objprostatus.cPesFailed = reader.GetDouble("cTotal") > 0 ? Convert.ToDouble((reader.GetDouble("cFailed") / reader.GetDouble("cTotal") * 100).ToString("0.##")) : 0;
                        objprostatus.cPesReject = reader.GetDouble("cTotal") > 0 ? Convert.ToDouble((reader.GetDouble("cReject") / reader.GetDouble("cTotal") * 100).ToString("0.##")) : 0;
                        objprostatus.cPesRetest = reader.GetDouble("cTotal") > 0 ? Convert.ToDouble((reader.GetDouble("cRetest") / reader.GetDouble("cTotal") * 100).ToString("0.##")) : 0;
                        objprostatus.cPesNoGlove = reader.GetDouble("cTotal") > 0 ? Convert.ToDouble((reader.GetDouble("cNoGlove") / reader.GetDouble("cTotal") * 100).ToString("0.##")) : 0;

                    }
                    connection.Close();

                    string json = JsonConvert.SerializeObject(objprostatus, objCSetting.JsonFormatdate());
                    return json;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return "";
            }
        }
        // GET api/<MachineInfoController>/5
        //Get Machine Batch
        [HttpGet("GetBatch/{batchno}")]
        public string GetBatch(string batchno)
        {
            if (batchno != "None")
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var batchlist = unipac_CmuContext.ViewOpBatchanalyses.ToList().Where(x => x.BatchNo == batchno).FirstOrDefault();
                if (batchlist == null)
                {
                    batchlist = new ViewOpBatchanalysis();
                    batchlist.BatchNo = "";
                    batchlist.Efficiency = 0;
                    batchlist.Failed = 0;
                    batchlist.Passed = 0;
                    batchlist.Retest = 0;
                    batchlist.NoGlove = 0;
                    batchlist.Total = 0;
                    batchlist.Rate = 0;
                    batchlist.RoundTime = 0;
                }
                string json = JsonConvert.SerializeObject(batchlist, objCSetting.JsonFormatdate());
                return json;
            }
            else
            {
                ViewOpBatchanalysis batchlist = new ViewOpBatchanalysis();
                batchlist.BatchNo = "";
                batchlist.Efficiency = 0;
                batchlist.Failed = 0;
                batchlist.Passed = 0;
                batchlist.Retest = 0;
                batchlist.NoGlove = 0;
                batchlist.Total = 0;
                batchlist.Rate = 0;
                batchlist.RoundTime = 0;
                string json = JsonConvert.SerializeObject(batchlist, objCSetting.JsonFormatdate());
                return json;
            }
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Batch List
        [HttpGet("GetBatchList/{id}")]
        public async Task<string> GetBatchListAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    using var connection = new MySqlConnection(csb.ConnectionString);
                    await connection.OpenAsync();
                    string Query = string.Format(@"SELECT * FROM tblbatchrecord");
                    MySqlCommand cmd = new MySqlCommand(Query, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    List<tblbatchrecord> objBatch = new List<tblbatchrecord>();
                    while (await reader.ReadAsync())
                    {
                        objBatch.Add(new tblbatchrecord()
                        {

                            cBatchNo = reader.GetString("cBatchNo"),
                            cStarttime = reader.GetDateTime("cStarttime"),
                            cEndtime = reader.GetDateTime("cEndtime"),
                            cProductType = reader.GetString("cProductType"),
                            cProductSize = reader.GetString("cProductSize"),
                            cRate = reader.GetDouble("cRate"),
                            cGloveType = reader.GetString("cGloveType"),
                            cOperator1_ID = reader.GetString("cOperator1_ID"),
                            cOperator2_ID = reader.GetString("cOperator2_ID"),
                            cOperator1_Name = reader.GetString("cOperator1_Name"),
                            cOperator2_Name = reader.GetString("cOperator2_Name"),
                            cInspector_ID = reader.IsDBNull("cInspector_ID") ? "-" : reader.GetString("cInspector_ID"),
                            cInspector_Name = reader.IsDBNull("cInspector_Name") ? "-" : reader.GetString("cInspector_Name"),
                            cPassed_Op = reader.GetInt32("cPassed_Op1") + reader.GetInt32("cPassed_Op2"),
                            cFailed_Op = reader.GetInt32("cFailed_Op1") + reader.GetInt32("cFailed_Op2"),
                            cReject_Op = reader.GetInt32("cReject_Op1") + reader.GetInt32("cReject_Op2"),
                            cRetest_Op = reader.GetInt32("cRetest_Op1") + reader.GetInt32("cRetest_Op2"),
                            cNoGlove_Op = reader.GetInt32("cNoGlove_Op1") + reader.GetInt32("cNoGlove_Op2"),

                        });
                    }
                    connection.Close();
                    string json = JsonConvert.SerializeObject(objBatch);
                    return json;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Batch List
        [HttpGet("GetBatchSingle/{id}/{batchno}")]
        public string GetBatchSingle(int id, string batchno)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var batchlist = unipac_CmuContext.ViewOpProductioninfos.ToList().Where(x => x.MachineId == id && x.BatchNo == batchno).OrderByDescending(x => x.CreateDate).FirstOrDefault();
            string json = JsonConvert.SerializeObject(batchlist, objCSetting.JsonFormatdate());
            return json;
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Event Log
        [HttpGet("GetEventLog/{id}")]
        public async Task<string> GetEventLogAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    using var connection = new MySqlConnection(csb.ConnectionString);
                    await connection.OpenAsync();
                    string Query = string.Format(@"SELECT * FROM tbleventlog");
                    MySqlCommand cmd = new MySqlCommand(Query, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    List<EventLog> objEvent = new List<EventLog>();
                    while (await reader.ReadAsync())
                    {
                        objEvent.Add(new EventLog()
                        {
                            cDateTime = reader.GetDateTime("cDateTime"),
                            cEvent = reader.GetString("cEvent")
                        });
                    }
                    connection.Close();

                    string json = JsonConvert.SerializeObject(objEvent);
                    return json;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return "";
            }
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Event Log
        [HttpGet("GetModuleLog/{id}")]
        public string GetModuleLog(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var eventList = unipac_CmuContext.TblOpMachinefaultlogs.ToList().Where(x => x.MachineId == id);
            string json = JsonConvert.SerializeObject(eventList);
            return json;
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Event Log
        [HttpGet("GetCallibrationList/{id}")]
        public string GetCallibrationList(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var eventList = unipac_CmuContext.TblOpCalibrations.ToList().Where(x => x.MachineId == id.ToString());
            string json = JsonConvert.SerializeObject(eventList, objCSetting.JsonFormatdate());
            return json;
        }

        // GET api/<MachineInfoController>/5
        //Get Machine Event Log
        [HttpGet("GetLicenseList/{id}")]
        public string GetLicenseList(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var eventList = unipac_CmuContext.TblOpLicenses.ToList().Where(x => x.MachineId == id.ToString());
            string json = JsonConvert.SerializeObject(eventList, objCSetting.JsonFormatdate());
            return json;
        }

        // POST api/<MachineInfoController>
        [HttpPost("SaveMachine")]
        public IActionResult Post([FromBody] TblOpMachineinfo MachineInfo)
        {
            try
            {
                //Make Clean on data
                TblOpMachineinfo objMachine = new TblOpMachineinfo();
                objMachine = MachineInfo;
                objMachine.Calibrationdate = DateTime.Now.AddYears(1).ToString();
                objMachine.Installationdate = DateTime.Now.ToString();
                objMachine.Recordstatus = "Off";
                objMachine.Createdate = DateTime.Now;
                objMachine.Modifydate = DateTime.Now;
                objMachine.Version = "4.1";

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblOpMachineinfos.Add(objMachine);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} machine created by {1}", objMachine.SerialNumber, objCSetting.GetUserName(objMachine.Createuserid));
                objCSetting.Auditlog(objMachine.Createuserid, Activity);

                int id = objMachine.Id;
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }

        // POST api/<MachineInfoController>
        [HttpPost("RemoveMachine/{machineid}")]
        public IActionResult RemoveMachine(int MachineID)
        {

            try
            {
                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.ToList().Where(x => x.Id == MachineID);
                MachineList.First<TblOpMachineinfo>().Recordstatus = "Inactive";
                MachineList.First<TblOpMachineinfo>().Modifydate = DateTime.Now;

                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} machine had removed", MachineList.First<TblOpMachineinfo>().SerialNumber);
                objCSetting.Auditlog(1, Activity);

                //int id = objMachine.Id;
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }

        [HttpPost("SaveCallibration")]
        public IActionResult UploadProject(IFormCollection file, string VersionInfo)
        {
            if (file != null)
            {
                try {
                    Microsoft.Extensions.Primitives.StringValues strVersionInfo = "";
                    file.TryGetValue("VersionInfo", out strVersionInfo);
                    TblOpCalibration objCallibration = new TblOpCalibration();
                    objCallibration = JsonConvert.DeserializeObject<TblOpCalibration>(strVersionInfo[0]);
                    string strFileName = string.Empty;
                    foreach (FormFile F in file.Files)
                    {
                        string fileDir = AppDomain.CurrentDomain.BaseDirectory + "Callibration\\Machine-" + objCallibration.MachineId;
                        if (!Directory.Exists(fileDir))
                        {
                            Directory.CreateDirectory(fileDir);
                        }
                        //file name 
                        strFileName = F.FileName;

                        //Path of the uploaded file
                        string filePath = fileDir + $@"\{DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName}";
                        using (FileStream fs = System.IO.File.Create(filePath))
                        {
                            F.CopyTo(fs);
                            fs.Flush();
                        }

                        objCallibration.FileName = DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName;
                        objCallibration.FilePath = fileDir;
                    }

                    objCallibration.RecordStatus = "Active";
                    objCallibration.CreateDate = DateTime.Now;
                    objCallibration.ModifyDate = DateTime.Now;

                    unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                    unipac_CmuContext.TblOpCalibrations.Add(objCallibration);
                    unipac_CmuContext.SaveChanges();

                    return this.Ok();
                } catch (Exception ex) {
                    return this.NotFound();
                }
                
            }
            else
            {
                return this.NotFound();
            }
        }

        [HttpPost("SaveLicense")]
        public IActionResult UploadProjectLicense(IFormCollection file, string VersionInfo)
        {
            if (file != null)
            {
                try
                {
                    Microsoft.Extensions.Primitives.StringValues strVersionInfo = "";
                    file.TryGetValue("VersionInfo", out strVersionInfo);
                    TblOpLicense objLicense = new TblOpLicense();
                    objLicense = JsonConvert.DeserializeObject<TblOpLicense>(strVersionInfo[0]);
                    string strFileName = string.Empty;
                    foreach (FormFile F in file.Files)
                    {
                        string fileDir = AppDomain.CurrentDomain.BaseDirectory + "License\\Machine-" + objLicense.MachineId;
                        if (!Directory.Exists(fileDir))
                        {
                            Directory.CreateDirectory(fileDir);
                        }
                        //file name 
                        strFileName = F.FileName;

                        //Path of the uploaded file
                        string filePath = fileDir + $@"\{DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName}";
                        using (FileStream fs = System.IO.File.Create(filePath))
                        {
                            F.CopyTo(fs);
                            fs.Flush();
                        }

                        objLicense.FileName = DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName; ;
                        objLicense.FilePath = fileDir;
                    }

                    objLicense.RecordStatus = "Active";
                    objLicense.CreateDate = DateTime.Now;
                    objLicense.ModifyDate = DateTime.Now;

                    unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                    unipac_CmuContext.TblOpLicenses.Add(objLicense);
                    unipac_CmuContext.SaveChanges();

                    return this.Ok();
                }
                catch (Exception ex)
                {
                    return this.NotFound();
                }

            }
            else
            {
                return this.NotFound();
            }
        }
    }
}
