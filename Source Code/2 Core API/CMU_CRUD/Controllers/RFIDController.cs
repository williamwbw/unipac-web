﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CMU_CRUD.Lib;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RFIDController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("GetRFIDList/{id}")]
        public string Get(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var RFIDList = unipac_CmuContext.ViewOpRfidinfos.ToList().Where(x => x.Recordstatus != "Inactive" && x.Companyid == id);

            string json = JsonConvert.SerializeObject(RFIDList, objCSetting.JsonFormatdate());
            return json;
        }


        // POST api/<EmployeeSaveController>
        [HttpPost("SaveRFID")]
        public IActionResult Post([FromBody] TblOpRfid RFIDInfo)
        {
            try
            {
                //Make Clean on data
                TblOpRfid objRFID = new TblOpRfid();
                objRFID = RFIDInfo;
                objRFID.RecordStatus = "Active";
                objRFID.CreateDate = DateTime.Now;
                objRFID.ModifyDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblOpRfids.Add(objRFID);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"RFID Card: {0} assigned to {1} by {2}", objRFID.CardId, objRFID.EmployeeName, objCSetting.GetUserName(objRFID.CreateUserId));
                objCSetting.Auditlog(objRFID.CreateUserId, Activity);

                int id = objRFID.Id;
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }
    }

}
