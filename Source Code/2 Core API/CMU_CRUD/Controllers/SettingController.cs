﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CMU_CRUD.Lib;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("GetProductSetting/{id}")]
        public string Get(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var ProductSettingList = unipac_CmuContext.TblOpProductsettings.ToList().Where(x => x.RecordStatus != "Inactive" && x.CompanyId == id);
            
            string json = JsonConvert.SerializeObject(ProductSettingList, objCSetting.JsonFormatdate());
            return json;
        }

        [HttpGet("GetRateSetting/{id}")]
        public string GetRate(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var RateSettingList = unipac_CmuContext.TblCmRatesettings.ToList().Where(x => x.RecordStatus != "Inactive" && x.CompanyId == id).OrderByDescending(x => x.ModifyDate).FirstOrDefault();

            string json = JsonConvert.SerializeObject(RateSettingList, objCSetting.JsonFormatdate());
            return json;
        }

        // POST api/<SaveProductSettingController>
        [HttpPost("SaveProductSetting")]
        public IActionResult Post([FromBody] TblOpProductsetting ProductSetting)
        { 
            try
            {
                //Make Clean on data
                TblOpProductsetting objProduct = new TblOpProductsetting();
                objProduct = ProductSetting;
                objProduct.RecordStatus = "Active";
                objProduct.CreateDate = DateTime.Now;
                objProduct.ModifyDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblOpProductsettings.Add(objProduct);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} product created by {1}", objProduct.ProductTitle, objCSetting.GetUserName(objProduct.CreateUserId));
                objCSetting.Auditlog(objProduct.CreateUserId, Activity);

                int id = objProduct.Id;
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }

        // POST api/<SaveProductSettingController>
        [HttpPost("SaveRateSetting")]
        public IActionResult SaveRateSetting([FromBody] TblCmRatesetting RateSetting)
        {
            try
            {
                //Make Clean on data
                TblCmRatesetting objRate = new TblCmRatesetting();
                objRate = RateSetting;
                objRate.RecordStatus = "Active";
                objRate.CreateDate = DateTime.Now;
                objRate.ModifyDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblCmRatesettings.Add(objRate);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"Rate updated by {0}", objCSetting.GetUserName(objRate.CreateUserId));
                objCSetting.Auditlog(objRate.CreateUserId, Activity);

                int id = objRate.Id;
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }
    }
}
