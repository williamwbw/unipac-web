﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CMU_CRUD.Lib;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("GetVersionList/{id}")]
        public string Get(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var UserList = unipac_CmuContext.TblOpVersions.ToList().Where(x => x.RecordStatus != "Inactive" && x.CompanyId == id);

            string json = JsonConvert.SerializeObject(UserList, objCSetting.JsonFormatdate());
            return json;
        }

        [HttpPost("SaveVersion")]
        public IActionResult UploadProject(IFormCollection file, string VersionInfo)
        {
            if (file != null)
            {
                Microsoft.Extensions.Primitives.StringValues strVersionInfo = "";
                file.TryGetValue("VersionInfo", out strVersionInfo);
                TblOpVersion objVersion = new TblOpVersion();
                objVersion = JsonConvert.DeserializeObject<TblOpVersion>(strVersionInfo[0]);
                string GenerationTag = objVersion.VersionGeneration == 1 ? "V4" : "V5";

                foreach (FormFile F in file.Files)
                {
                    var fileDir = AppDomain.CurrentDomain.BaseDirectory + "Version\\" + objVersion.CompanyId + "\\" + GenerationTag + "\\" + objVersion.VersionName;
                    if (!Directory.Exists(fileDir))
                    {
                        Directory.CreateDirectory(fileDir);
                    }
                    //file name 
                    string strFileName = F.FileName;
                    string[] lstFileExtension = strFileName.Split('.');
                    switch (lstFileExtension[1])
                    {
                        case "exe":
                            objVersion.FileNameExe = fileDir + "\\" +  DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName;
                            break;
                        case "aliases":
                            objVersion.FileNameAliases = fileDir + "\\" + DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName;
                            break;
                        case "ini":
                            objVersion.FileNameIni = fileDir + "\\" + DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName;
                            break;
                    }

                    //Path of the uploaded file
                    string filePath = fileDir + $@"\{DateTime.Today.ToString("ddmmyyyy") + "_" + strFileName}";
                    using (FileStream fs = System.IO.File.Create(filePath))
                    {
                        F.CopyTo(fs);
                        fs.Flush();
                    }
                }

                objVersion.RecordStatus = "Active";
                objVersion.CreateDate = DateTime.Now;
                objVersion.ModifyDate = DateTime.Now;

                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblOpVersions.Add(objVersion);
                unipac_CmuContext.SaveChanges();

                return this.Ok();
            }
            else
            {
                return this.NotFound();
            }
        }

    }
}
