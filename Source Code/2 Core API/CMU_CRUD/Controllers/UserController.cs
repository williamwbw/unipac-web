﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CMU_CRUD.Lib;
using Newtonsoft.Json.Linq;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("GetManagerList/{id}")]
        public string Get(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var UserList = unipac_CmuContext.ViewCmUsers.ToList().Where(x => x.RecordStatus != "Inactive" && x.CompanyId == id && x.UserRole > 1);

            string json = JsonConvert.SerializeObject(UserList, objCSetting.JsonFormatdate());
            return json;
        }

        [HttpGet("GetAuditLog/{id}")]
        public string GetAuditLog(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var UserList = unipac_CmuContext.TblCmAuditlogs.ToList().Where(x => x.UserId == id).OrderByDescending(x=>x.CreateDate);

            string json = JsonConvert.SerializeObject(UserList, objCSetting.JsonFormatdate());
            return json;
        }

        [HttpPost("UpdatePassword")]
        public string UpdatePassword([FromBody] TblCmUser UserLogin)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var UserList = unipac_CmuContext.TblCmUsers.Where(x => x.RecordStatus != "Inactive" && x.Email == UserLogin.Email).FirstOrDefault();
                UserList.Password = UserLogin.Password;

                unipac_CmuContext.SaveChanges();

                return "";
            }catch(Exception ex)
            {
                return "Update Password Failed";
            }
        }

        // POST api/<EmployeeSaveController>
        [HttpPost("ResetPassword/{email}")]
        public string ResetPassword(string email)
        {
            try
            {
                //Collect user Login information
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var UserList = unipac_CmuContext.TblCmUsers.Where(x => x.RecordStatus != "Inactive" && x.Email == email);
                if (UserList.Count() > 0)
                {
                    string Activity = string.Format(@"{0} Reseted Password", UserList.First<TblCmUser>().Username);
                    objCSetting.Auditlog(UserList.First<TblCmUser>().Id, Activity);

                    string HTMLTemplate = @"
                                            <!DOCTYPE html>
                                            <html>
                                            <head>

                                              <meta charset=""utf-8"">
                                              <meta http-equiv=""x-ua-compatible"" content=""ie=edge"">
                                              <title>Password Reset</title>
                                              <meta name=""viewport"" content=""width=device-width, initial-scale=1"">
                                              <style type=""text/css"">
                                              /**
                                               * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
                                               */
                                              @media screen {
                                                @font-face {
                                                  font-family: 'Source Sans Pro';
                                                  font-style: normal;
                                                  font-weight: 400;
                                                  src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
                                                }

                                                @font-face {
                                                  font-family: 'Source Sans Pro';
                                                  font-style: normal;
                                                  font-weight: 700;
                                                  src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
                                                }
                                              }

                                              /**
                                               * Avoid browser level font resizing.
                                               * 1. Windows Mobile
                                               * 2. iOS / OSX
                                               */
                                              body,
                                              table,
                                              td,
                                              a {
                                                -ms-text-size-adjust: 100%; /* 1 */
                                                -webkit-text-size-adjust: 100%; /* 2 */
                                              }

                                              /**
                                               * Remove extra space added to tables and cells in Outlook.
                                               */
                                              table,
                                              td {
                                                mso-table-rspace: 0pt;
                                                mso-table-lspace: 0pt;
                                              }

                                              /**
                                               * Better fluid images in Internet Explorer.
                                               */
                                              img {
                                                -ms-interpolation-mode: bicubic;
                                              }

                                              /**
                                               * Remove blue links for iOS devices.
                                               */
                                              a[x-apple-data-detectors] {
                                                font-family: inherit !important;
                                                font-size: inherit !important;
                                                font-weight: inherit !important;
                                                line-height: inherit !important;
                                                color: inherit !important;
                                                text-decoration: none !important;
                                              }

                                              /**
                                               * Fix centering issues in Android 4.4.
                                               */
                                              div[style*=""margin: 16px 0;""] {
                                                margin: 0 !important;
                                              }

                                              body {
                                                width: 100% !important;
                                                height: 100% !important;
                                                padding: 0 !important;
                                                margin: 0 !important;
                                              }

                                              /**
                                               * Collapse table borders to avoid space between cells.
                                               */
                                              table {
                                                border-collapse: collapse !important;
                                              }

                                              a {
                                                color: #1a82e2;
                                              }

                                              img {
                                                height: auto;
                                                line-height: 100%;
                                                text-decoration: none;
                                                border: 0;
                                                outline: none;
                                              }
                                              </style>

                                            </head>
                                            <body style=""background-color: #e9ecef;"">
                                              <!-- start body -->
                                              <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">

                                                <!-- start logo -->
                                                <tr>
                                                  <td align=""center"" bgcolor=""#e9ecef"">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
                                                    <tr>
                                                    <td align=""center"" valign=""top"" width=""600"">
                                                    <![endif]-->
                                                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""max-width: 600px;"">
                                                      <tr>
                                                        <td align=""center"" valign=""top"" style=""padding: 36px 24px;"">
                                                            <img src=""	http://118.107.242.221:8888/img/QMAX-Logo.276fa402.png"" alt=""Logo"" border=""0"" width=""48"" style=""display: block; width: 400px; max-width: 500px; min-width: 48px;"">
                                                        </td>
                                                      </tr>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                  </td>
                                                </tr>
                                                <!-- end logo -->

                                                <!-- start hero -->
                                                <tr>
                                                  <td align=""center"" bgcolor=""#e9ecef"">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
                                                    <tr>
                                                    <td align=""center"" valign=""top"" width=""600"">
                                                    <![endif]-->
                                                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""max-width: 600px;"">
                                                      <tr>
                                                        <td align=""left"" bgcolor=""#ffffff"" style=""padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;"">
                                                          <h1 style=""margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;"">Reset Your Password</h1>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                  </td>
                                                </tr>
                                                <!-- end hero -->

                                                <!-- start copy block -->
                                                <tr>
                                                  <td align=""center"" bgcolor=""#e9ecef"">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
                                                    <tr>
                                                    <td align=""center"" valign=""top"" width=""600"">
                                                    <![endif]-->
                                                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""max-width: 600px;"">

                                                      <!-- start copy -->
                                                      <tr>
                                                        <td align=""left"" bgcolor=""#ffffff"" style=""padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"">
                                                          <p style=""margin: 0;"">Please key in the new password for your login account. Please do not share your password to others. Thank you!</p>
                                                        </td>
                                                      </tr>
                                                      <!-- end copy -->

                                                      <!-- start button -->
                                                      <tr>
                                                        <td align=""left"" bgcolor=""#ffffff"">
                                                          <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                                            <tr>
                                                              <td align=""center"" bgcolor=""#ffffff"" style=""padding: 12px;"">
                                                                <table border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                                  <tr>
                                                                    <td align=""center"" bgcolor=""#1a82e2"" style=""border-radius: 6px;"">";
                    HTMLTemplate += @"<a href=""http://118.107.242.221:8888/UpdatePassword/" + email;
                    HTMLTemplate += @""" target=""_blank"" style=""display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;"">Click Here To Reset Your Password</a>";
                    HTMLTemplate += @"
                                                                    </td>
                                                                  </tr>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <!-- end button -->

                                                    </table>
                                                  </td>
                                                </tr>
                                                <!-- end copy block -->

                                                <!-- start footer -->
                                                <tr>
                                                  <td align=""center"" bgcolor=""#e9ecef"" style=""padding: 24px;"">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    <table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""600"">
                                                    <tr>
                                                    <td align=""center"" valign=""top"" width=""600"">
                                                    <![endif]-->
                                                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""max-width: 600px;"">

                                                      <!-- start permission -->
                                                      <tr>
                                                        <td align=""center"" bgcolor=""#e9ecef"" style=""padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;"">
                                                          <p style=""margin: 0;"">You received this email because we received a request for reset password for your account. If you didn't request reset password you can safely delete this email.</p>
                                                        </td>
                                                      </tr>
                                                      <!-- end permission -->

                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                  </td>
                                                </tr>
                                                <!-- end footer -->

                                              </table>
                                              <!-- end body -->

                                            </body>
                                            </html>";
                    objCSetting.MailSend(HTMLTemplate, UserList.First<TblCmUser>().Email);

                    return "Mail Sended";

                }
                else
                {
                    return "Username / Password Incorrect";
                }
            }
            catch (Exception ex)
            {
                return "Username / Password Incorrect";
            }
        }

        // POST api/<EmployeeSaveController>
        [HttpPost("Login")]
        public string Post([FromBody] TblCmUser UserLogin)
        {
            try
            {
                //Collect user Login information
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var UserList = unipac_CmuContext.TblCmUsers.Where(x => x.RecordStatus != "Inactive" && x.Email == UserLogin.Username && x.Password == UserLogin.Password);
                if (UserList.Count() > 0)
                {
                    UserList.First<TblCmUser>().RecordStatus = "Online";
                    UserList.First<TblCmUser>().LastLoginDate = DateTime.Now;

                    ////Ready to save into DB
                    unipac_CmuContext.SaveChanges();

                    string Activity = string.Format(@"{0} Login to CMU", UserLogin.Username);
                    objCSetting.Auditlog(UserList.First<TblCmUser>().Id, Activity);

                    //int id = objUserLogin.Id;
                    var UserInfoList = unipac_CmuContext.ViewCmUsers.ToList().Where(x => x.RecordStatus != "Inactive" && x.Email == UserLogin.Username && x.Password == UserLogin.Password);
                    string json = JsonConvert.SerializeObject(UserInfoList, objCSetting.JsonFormatdate());
                    return json;
                }
                else
                {
                    return "Username / Password Incorrect";
                }
            }
            catch (Exception ex)
            {
                return "Username / Password Incorrect";
            }
        }

        [HttpPost("RemoveUser/{id}")]
        public IActionResult RemoveUser(int id)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var User = unipac_CmuContext.TblCmUsers.Where(x => x.Id == id).FirstOrDefault();
                User.RecordStatus = "Inactive";
                User.ModifyDate = DateTime.Now;
                unipac_CmuContext.SaveChanges();
                return Ok();
            }catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("CreateUser")]
        public IActionResult CreateUser([FromBody] TblCmUser UserLogin)
        {
            try
            {
                //Make Clean on data
                TblCmUser objUser = new TblCmUser();
                objUser = UserLogin;
                objUser.Username = objUser.Email;
                objUser.OperationLevel = 0;
                objUser.Password = "p@ssw0rd";
                objUser.RecordStatus = "Off";
                objUser.CreateDate = DateTime.Now;
                objUser.ModifyDate = DateTime.Now;
                objUser.LastLoginDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblCmUsers.Add(objUser);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} account created", UserLogin.Username);
                objCSetting.Auditlog(objUser.Id, Activity);

                Activity = string.Format(@"{0} created account for {1}", objCSetting.GetUserName(objUser.CreateUserId), UserLogin.Username);
                objCSetting.Auditlog(objUser.CreateUserId, Activity);

                int id = objUser.Id;
                string messageBody = "<font>Here is the account been created: </font><br><br>";
                string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
                string htmlTableEnd = "</table>";
                string htmlHeaderRowStart = "<tr style=\"background-color:#6FA1D2; color:#ffffff;\">";
                string htmlHeaderRowEnd = "</tr>";
                string htmlTrStart = "<tr style=\"color:#555555;\">";
                string htmlTrEnd = "</tr>";
                string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px;\">";
                string htmlTdEnd = "</td>";
                messageBody += htmlTableStart;
                messageBody += htmlHeaderRowStart;
                messageBody += htmlTdStart + "User Accont" + htmlTdEnd;
                messageBody += htmlTdStart + "Password" + htmlTdEnd;
                messageBody += htmlTdStart + "Email" + htmlTdEnd;
                messageBody += htmlTdStart + "Access URL" + htmlTdEnd;
                messageBody += htmlHeaderRowEnd;
                messageBody = messageBody + htmlTrStart;
                messageBody = messageBody + htmlTdStart + objUser.Username + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + objUser.Password + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + objUser.Email + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + "http://localhost:8080" + htmlTdEnd;
                messageBody = messageBody + htmlTrEnd;
                messageBody = messageBody + htmlTableEnd;
                objCSetting.MailSend(messageBody, objUser.Email);
            }
            catch (Exception ex)
            {
                string Activity = string.Format(@"error at registration: {0}", ex.Message);
                objCSetting.Auditlog(0, Activity);
            }
            return Ok();
        }
    }
}
