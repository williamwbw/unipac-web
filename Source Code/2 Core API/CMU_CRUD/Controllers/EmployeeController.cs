﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using CMU_CRUD.Lib;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("{id}")]
        public string Get(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var EmployeeList = unipac_CmuContext.TblCmEmployees.ToList().Where(x => x.RecordStatus != "Inactive" && x.CompanyId == id).OrderByDescending(x => x.EmployeeId);
            string json = JsonConvert.SerializeObject(EmployeeList);
            return json;
        }        
        
        // POST api/<EmployeeSaveController>
        [HttpPost("SaveEmployee")]
        public IActionResult Post([FromBody] TblCmEmployee EmployeeInfo)
        {

            try
            {
                //Make Clean on data
                TblCmEmployee objEmployee = new TblCmEmployee();
                objEmployee = EmployeeInfo;
                objEmployee.RecordStatus = "Active";
                objEmployee.CreateDate = DateTime.Now;
                objEmployee.ModifyDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblCmEmployees.Add(objEmployee);
                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} operator created by {1}", objEmployee.EmployeeName, objCSetting.GetUserName(objEmployee.CreateUserId));
                objCSetting.Auditlog(objEmployee.CreateUserId, Activity);

                int id = objEmployee.Id;
            }
            catch (Exception ex)
            {
                string Activity = string.Format(@"error at operator: {0}", ex.Message);
                objCSetting.Auditlog(0, Activity);
            }
            return this.Ok();
        }
    }
}
