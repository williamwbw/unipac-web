﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using System.Security.Cryptography;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadController : ControllerBase
    {
        [HttpGet("GetDownloadFile/{serialnumber}/{type}")]
        public async Task<IActionResult> GetDownloadFile(string serialnumber, string type)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.SerialNumber == serialnumber && x.Recordstatus != "Inactive").FirstOrDefault();
                WriteToFile(DateTime.Now + ": Download Step 1 : Getting Machine Information");
                string MachineID = MachineList != null ? MachineList.Id.ToString() : "";
                WriteToFile(DateTime.Now + ": Download Step 2 : Getting Machine Information, Machine ID = " + MachineID);
                string fildir = string.Empty;
                switch (type.ToLower())
                {
                    case "calibration":
                        var CalibrationList = unipac_CmuContext.TblOpCalibrations.Where(x => x.MachineId == MachineID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();

                        fildir = CalibrationList.FilePath.Replace("\\", "\\\\") + "\\\\" + CalibrationList.FileName;
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        
                        break;                    
                    case "license":
                        var LicenseList = unipac_CmuContext.TblOpLicenses.Where(x => x.MachineId == MachineID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();

                        fildir = LicenseList.FilePath.Replace("\\", "\\\\") + "\\\\" + LicenseList.FileName;
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);

                        break;
                }
                FileStream fs = System.IO.File.OpenRead(fildir);
                Stream stream = fs;
                return File(fs, "application/octet-stream");
            }
            catch(Exception ex)
            {
                WriteToFile(DateTime.Now + ": Download Issue : " + ex.InnerException.Message);

                return (IActionResult)ex;
            }
        }


        [HttpGet("GetRFIDBySerialNumber/{serialnumber}")]
        public async Task<IActionResult> GetRFIDBySerialNumber(string serialnumber)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.SerialNumber == serialnumber && x.Recordstatus != "Inactive").FirstOrDefault();
                WriteToFile(DateTime.Now + ": Download Step 1 : Getting Machine Information");
                int CompanyID = MachineList != null ? MachineList.CompanyId : 0;
                WriteToFile(DateTime.Now + ": Download Step 2 : Getting Machine Information, Company ID = " + CompanyID);
                string fildir = string.Empty;
                string RFIDText = string.Empty;
                var RFIDList = unipac_CmuContext.TblOpRfids.ToList().Where(x => x.CompanyId == CompanyID && x.RecordStatus != "Inactive");
                foreach(var RFID in RFIDList)
                {
                    var employeename = new string(RFID.EmployeeName.ToCharArray().Reverse().ToArray());
                    var RFIDCardID = new string(RFID.CardId.ToCharArray().Reverse().ToArray());
                    
                    RFIDText += "[" + ASCIIToOctal(stringToASCII(employeename).TrimEnd(' ')) + "]";
                    RFIDText += Environment.NewLine;
                    RFIDText += ASCIIToOctal(stringToASCII("Name").TrimEnd(' ')) + "=" + ASCIIToOctal(stringToASCII(employeename).TrimEnd(' '));
                    RFIDText += Environment.NewLine;
                    RFIDText += ASCIIToOctal(stringToASCII("CardID").TrimEnd(' ')) + "=" + ASCIIToOctal(stringToASCII(RFIDCardID).TrimEnd(' '));
                    RFIDText += Environment.NewLine;
                    RFIDText += ASCIIToOctal(stringToASCII("AccessLevel").TrimEnd(' ')) + "=" + ASCIIToOctal(stringToASCII(RFID.Oplevel.ToString()).TrimEnd(' '));
                    RFIDText += Environment.NewLine;
                    RFIDText += Environment.NewLine;
                }

                string path = AppDomain.CurrentDomain.BaseDirectory + "\\test\\RFID.txt";
                FileInfo fi = new FileInfo(path);
                using (FileStream sw = fi.Create())
                {
                    Byte[] txt = new UTF8Encoding(true).GetBytes(RFIDText);
                    await sw.WriteAsync(txt);
                }

                return Ok();
            }catch(Exception ex)
            {
                WriteToFile(DateTime.Now + ": Download RFID ERRO : " + ex.Message);
                return NotFound(ex.Message);
            }
        }

        public string stringToASCII(string str)
        {
            string utfString = string.Empty;
            foreach (char c in str)
            {
                utfString += System.Convert.ToInt32(c).ToString() + " ";
            }
            return utfString;
        }

        public string ASCIIToOctal(string str)
        {
            string oct = string.Empty;

            for (int i = 0; i < str.Length; ++i)
            {
                string cOct = DecimalToOctal(str[i]);

                if (cOct.Length < 3)
                    cOct = cOct.PadLeft(3, '0');

                oct += cOct;
            }

            return oct;
        }

        private string DecimalToOctal(int dec)
        {
            if (dec < 1) return "0";

            string octStr = string.Empty;

            while (dec > 0)
            {
                octStr = octStr.Insert(0, (dec % 8).ToString());
                dec /= 8;
            }

            return octStr;
        }

        [HttpGet("GetFileName/{serialnumber}/{type}")]
        public string GetFileName(string serialnumber, string type)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.SerialNumber == serialnumber && x.Recordstatus != "Inactive").FirstOrDefault();
                WriteToFile(DateTime.Now + ": Download Step 1 : Getting Machine Information");
                string MachineID = MachineList != null ? MachineList.Id.ToString() : "";
                WriteToFile(DateTime.Now + ": Download Step 2 : Getting Machine Information, Machine ID = " + MachineID);
                string FileName = string.Empty;
                switch (type.ToLower())
                {
                    case "calibration":
                        var CalibrationList = unipac_CmuContext.TblOpCalibrations.Where(x => x.MachineId == MachineID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                        FileName = CalibrationList.FileName;
                        break;
                    case "license":
                        var LicenseList = unipac_CmuContext.TblOpLicenses.Where(x => x.MachineId == MachineID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                        FileName = LicenseList.FileName;
                        break;
                }

                return FileName;
            }
            catch (Exception ex)
            {
                WriteToFile(DateTime.Now + ": Download Issue : " + ex.InnerException.Message);

                return ex.InnerException.Message;
            }
        }

        [HttpGet("GetDownloadSoftware/{serialnumber}/{type}")]
        public async Task<IActionResult> GetDownloadSoftware(string serialnumber, string type)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.SerialNumber == serialnumber && x.Recordstatus != "Inactive").FirstOrDefault();
                WriteToFile(DateTime.Now + ": Download Step 1 : Getting Machine Information");
                int CompanyID = MachineList != null ? MachineList.CompanyId : 0;
                WriteToFile(DateTime.Now + ": Download Step 2 : Getting Machine Information, Company ID = " + CompanyID);
                string fildir = string.Empty;
                var exeList = unipac_CmuContext.TblOpVersions.Where(x => x.CompanyId == CompanyID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                switch (type.ToLower())
                {
                    case "exe":
                        fildir = exeList.FileNameExe.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                    case "aliases":
                        fildir = exeList.FileNameAliases.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                    case "ini":
                        fildir = exeList.FileNameIni.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                }
                FileStream fs = System.IO.File.OpenRead(fildir);
                Stream stream = fs;
                return File(fs, "application/octet-stream");
            }
            catch (Exception ex)
            {
                WriteToFile(DateTime.Now + ": Download Issue : " + ex.InnerException.Message);

                return (IActionResult)ex;
            }
        }

        [HttpGet("GetSoftwareName/{serialnumber}/{type}")]
        public string GetSoftwareName(string serialnumber, string type)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.SerialNumber == serialnumber && x.Recordstatus != "Inactive").FirstOrDefault();
                WriteToFile(DateTime.Now + ": Download Step 1 : Getting Machine Information");
                int CompanyID = MachineList != null ? MachineList.CompanyId : 0;
                WriteToFile(DateTime.Now + ": Download Step 2 : Getting Machine Information, Company ID = " + CompanyID);
                string fildir = string.Empty;
                var exeList = unipac_CmuContext.TblOpVersions.Where(x => x.CompanyId == CompanyID && x.RecordStatus != "Inactive").OrderByDescending(x => x.CreateDate).FirstOrDefault();
                string FileName = string.Empty;
                switch (type.ToLower())
                {
                    case "exe":
                        fildir = exeList.FileNameExe.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                    case "aliases":
                        fildir = exeList.FileNameAliases.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                    case "ini":
                        fildir = exeList.FileNameIni.Replace("\\", "\\\\");
                        WriteToFile(DateTime.Now + ": Download Step 3 : Getting Machine Information, File Directory = " + fildir);
                        break;
                }
                FileName = Path.GetFileName(fildir);
                return FileName;
            }
            catch (Exception ex)
            {
                WriteToFile(DateTime.Now + ": Download Issue : " + ex.InnerException.Message);

                return ex.InnerException.Message;
            }
        }


        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\DownloadFileLogs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\DownloadFileLogs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        private string GetContentType(string fileExtension)
        {
            if (string.IsNullOrEmpty(fileExtension))
                return string.Empty;

            string contentType = string.Empty;
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                    contentType = "text/HTML";
                    break;

                case ".txt":
                    contentType = "text/plain";
                    break;

                case ".doc":
                case ".rtf":
                case ".docx":
                    contentType = "Application/msword";
                    break;

                case ".xls":
                case ".xlsx":
                    contentType = "Application/x-msexcel";
                    break;

                case ".jpg":
                case ".jpeg":
                    contentType = "image/jpeg";
                    break;

                case ".gif":
                    contentType = "image/GIF";
                    break;

                case ".pdf":
                    contentType = "application/pdf";
                    break;
            }

            return contentType;
        }
    }
}
