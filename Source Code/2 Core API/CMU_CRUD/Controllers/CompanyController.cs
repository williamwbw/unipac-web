﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using CMU_CRUD.Lib;
using System.Net.Mail;
using System.Net;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();
        // GET api/<CompanyController>/5
        //Get Company Information List
        [HttpGet]
        public string Get()
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var CompanyList = unipac_CmuContext.TblOpCompanies.ToList().Where(x => x.RecordStatus != "Inactive");
            string json = JsonConvert.SerializeObject(CompanyList);
            return json;
        }

        [HttpPost("RemoveAccess/{id}")]
        public IActionResult RemoveAccess(int id)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();

                //Check for Machine Online
                var MachineList = unipac_CmuContext.TblOpMachineinfos.ToList().Where(x => x.CompanyId == id && x.Recordstatus != "Off" && x.Recordstatus != "Inactive");
                if(MachineList.Count() > 0)
                {
                    return NotFound();
                }

                var UpdateMachine = unipac_CmuContext.TblOpMachineinfos.ToList().Where(x => x.CompanyId == id && x.Recordstatus != "Inactive");
                foreach (var Machine in UpdateMachine)
                {
                    Machine.Recordstatus = "Inactive";
                }

                var CompanyList = unipac_CmuContext.TblOpCompanies.ToList().Where(x => x.Id == id).FirstOrDefault();
                CompanyList.RecordStatus = "Inactive";

                var UserList = unipac_CmuContext.TblCmUsers.ToList().Where(x => x.CompanyId == id).FirstOrDefault();
                UserList.RecordStatus = "Inactive";

                unipac_CmuContext.SaveChanges();

                string Activity = string.Format(@"{0} access had removed", CompanyList.CompanyName);
                objCSetting.Auditlog(1, Activity);

                return Ok();
            }catch(Exception ex)
            {
                return NotFound();
            }
        }

        [HttpPost("UpdateCompany")]
        public IActionResult UpdateCompany([FromBody] TblOpCompany CompanyInfo)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var Company = unipac_CmuContext.TblOpCompanies.Where(x => x.Id == CompanyInfo.Id && x.RecordStatus != "Inactive").FirstOrDefault();
                Company.CompanyName = CompanyInfo.CompanyName;
                Company.CompanyAddress = CompanyInfo.CompanyAddress;
                Company.MaxAccount = CompanyInfo.MaxAccount;

                unipac_CmuContext.SaveChanges();

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet("GetLocalCMU/{id}")]
        public string GetLocalCMU(int id)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var Company = unipac_CmuContext.TblOpLocalCmus.Where(x => x.CompanyId == id && x.RecordStatus != "Inactive").FirstOrDefault();
                string json = JsonConvert.SerializeObject(Company);
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("ActivateLocalCMU")]
        public IActionResult ActivateLocalCMU([FromBody] TblOpLocalCmu LocalCMU)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var Company = unipac_CmuContext.TblOpLocalCmus.Where(x => x.CompanyId == LocalCMU.CompanyId && x.RecordStatus != "Inactive").FirstOrDefault();
                if (Company != null)
                {
                    Company.LocalCmu = LocalCMU.LocalCmu;
                    Company.Sshport = LocalCMU.Sshport;
                    Company.SshuserName = LocalCMU.SshuserName;
                    Company.Dbserver = LocalCMU.Dbserver;
                    Company.Dbname = LocalCMU.Dbname;
                    Company.DbuserName = LocalCMU.DbuserName;
                    Company.Dbpassword = LocalCMU.Dbpassword;
                }
                else
                {
                    LocalCMU.RecordStatus = "Active";
                    LocalCMU.CreateDate = DateTime.Now;
                    LocalCMU.ModifyDate = DateTime.Now;
                    unipac_CmuContext.Add(LocalCMU);
                }
                var MachineList = unipac_CmuContext.TblOpMachineinfos.Where(x => x.CompanyId == LocalCMU.CompanyId && x.Recordstatus != "Inactive").ToList();
                foreach (var machine in MachineList)
                {
                    machine.Sshipaddress = LocalCMU.Sshipaddress;
                }

                unipac_CmuContext.SaveChanges();

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/<EmployeeSaveController>
        [HttpPost("CreateUser")]
        public IActionResult Post([FromBody] TblOpCompany CompanyInfo)
        {
            try
            {
                //Make Clean on data
                TblOpCompany objCompany = new TblOpCompany();
                objCompany = CompanyInfo;
                objCompany.RecordStatus = "Active";
                objCompany.CreateDate = DateTime.Now;
                objCompany.ModifyDate = DateTime.Now;

                //Ready to save into DB
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                unipac_CmuContext.TblOpCompanies.Add(objCompany);
                unipac_CmuContext.SaveChanges();

                int CompanyId = objCompany.Id;

                //After collect company ID, direct create account access

                TblCmUser objUser = new TblCmUser();
                objUser.Username = objCompany.Email;
                objUser.Email = objCompany.Email;
                objUser.UserRole = 1;
                objUser.OperationLevel = 0;
                objUser.CompanyId = CompanyId;
                objUser.EmployeeId = "0";
                objUser.Password = "p@ssw0rd";
                objUser.RecordStatus = "Off";
                objUser.CreateDate = DateTime.Now;
                objUser.ModifyDate = DateTime.Now;
                objUser.LastLoginDate = DateTime.Now;
                unipac_CmuContext.TblCmUsers.Add(objUser);
                unipac_CmuContext.SaveChanges();
                int UserID = objUser.Id;

                string messageBody = "<font>Here is the account been created: </font><br><br>";
                string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
                string htmlTableEnd = "</table>";
                string htmlHeaderRowStart = "<tr style=\"background-color:#6FA1D2; color:#ffffff;\">";
                string htmlHeaderRowEnd = "</tr>";
                string htmlTrStart = "<tr style=\"color:#555555;\">";
                string htmlTrEnd = "</tr>";
                string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px;\">";
                string htmlTdEnd = "</td>";
                messageBody += htmlTableStart;
                messageBody += htmlHeaderRowStart;
                messageBody += htmlTdStart + "Company Name" + htmlTdEnd;
                messageBody += htmlTdStart + "User Accont" + htmlTdEnd;
                messageBody += htmlTdStart + "Password" + htmlTdEnd;
                messageBody += htmlTdStart + "Email" + htmlTdEnd;
                messageBody += htmlTdStart + "Access URL" + htmlTdEnd;
                messageBody += htmlHeaderRowEnd;
                messageBody = messageBody + htmlTrStart;
                messageBody = messageBody + htmlTdStart + objCompany.CompanyName + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + objUser.Username + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + objUser.Password + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + objUser.Email + htmlTdEnd;
                messageBody = messageBody + htmlTdStart + "http://localhost:8080" + htmlTdEnd;
                messageBody = messageBody + htmlTrEnd;
                messageBody = messageBody + htmlTableEnd;

                objCSetting.MailSend(messageBody, objUser.Email);
            }
            catch (Exception ex)
            {

            }
            return this.Ok();
        }

    }
}
