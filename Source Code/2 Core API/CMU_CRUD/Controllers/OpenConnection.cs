﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace CMU_CRUD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OpenConnection : ControllerBase
	{
        private IConfiguration Configuration;

        public OpenConnection(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }

		public static (SshClient SshClient, uint Port) ConnectSsh(string sshHostName, string sshUserName, string sshPassword = null,
	string sshKeyFile = "C:\\Users\\William\\Desktop\\id_rsa", string sshPassPhrase = null, int sshPort = 5003, string databaseServer = "localhost", int databasePort = 3307)
		{

				// check arguments
				if (string.IsNullOrEmpty(sshHostName))
					throw new ArgumentException($"{nameof(sshHostName)} must be specified.", nameof(sshHostName));
				if (string.IsNullOrEmpty(sshHostName))
					throw new ArgumentException($"{nameof(sshUserName)} must be specified.", nameof(sshUserName));
				if (string.IsNullOrEmpty(sshPassword) && string.IsNullOrEmpty(sshKeyFile))
					throw new ArgumentException($"One of {nameof(sshPassword)} and {nameof(sshKeyFile)} must be specified.");
				if (string.IsNullOrEmpty(databaseServer))
					throw new ArgumentException($"{nameof(databaseServer)} must be specified.", nameof(databaseServer));

				// define the authentication methods to use (in order)
				var authenticationMethods = new List<AuthenticationMethod>();
			try
			{
				if (!string.IsNullOrEmpty(sshKeyFile))
				{
					authenticationMethods.Add(new PrivateKeyAuthenticationMethod(sshUserName,
						new PrivateKeyFile(sshKeyFile, string.IsNullOrEmpty(sshPassPhrase) ? null : sshPassPhrase)));
				}
				if (!string.IsNullOrEmpty(sshPassword))
				{
					authenticationMethods.Add(new PasswordAuthenticationMethod(sshUserName, sshPassword));
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			// connect to the SSH server
			var sshClient = new SshClient(new ConnectionInfo(sshHostName, sshPort, sshUserName, authenticationMethods.ToArray()));
				sshClient.Connect();

				// forward a local port to the database server and port, using the SSH server
				var forwardedPort = new ForwardedPortLocal("127.0.0.1", databaseServer, (uint)databasePort);
				sshClient.AddForwardedPort(forwardedPort);
				forwardedPort.Start();

				return (sshClient, forwardedPort.BoundPort);
		}

		[HttpGet]
		public async Task<string> IndexAsync()
        {
			try
			{
				var sshServer = "115.134.118.10";
				var sshUserName = "eBOX62x-312";
				var sshPassword = "";
				var databaseServer = "DESKTOP-A1DF98Q";
				var databaseUserName = "newuser";
				var databasePassword = "0123456789";
				string test = string.Empty;

				var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer);
				using (sshClient)
				{
					MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
					{
						Server = "127.0.0.1",
						Port = localPort,
						UserID = databaseUserName,
						Password = databasePassword,
						Database = "avx_unipac_qmax4",
					};

					using var connection = new MySqlConnection(csb.ConnectionString);
					connection.Open();
					connection.Close();
				}
				return test;
			}catch(Exception ex)
            {
				return "";
            }
		}
    }
}
