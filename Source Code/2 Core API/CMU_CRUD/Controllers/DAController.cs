﻿using CMU_CRUD.DBModels;
using CMU_CRUD.Lib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMU_CRUD.Models_QMAX;
using System.IO;

namespace CMU_CRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DAController : ControllerBase
    {
        CommonSetting objCSetting = new CommonSetting();

        [HttpGet("GetPassed/{id}/{year}")]
        public string GetPassed(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaPasseds.ToList().Where(x => x.Machineid == id && x.Years == year).FirstOrDefault();
            if(DAList == null)
            {
                DAList = new ViewDaPassed();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;
                
            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }        
        [HttpGet("GetFailed/{id}/{year}")]
        public string GetFailed(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaFaileds.ToList().Where(x => x.MachineId == id && x.Years == year).FirstOrDefault();
            if (DAList == null)
            {
                DAList = new ViewDaFailed();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;

            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }
        [HttpGet("GetReject/{id}/{year}")]
        public string GetReject(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaRejects.ToList().Where(x => x.MachineId == id && x.Years == year).FirstOrDefault();
            if (DAList == null)
            {
                DAList = new ViewDaReject();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;

            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }
        [HttpGet("GetRetest/{id}/{year}")]
        public string GetRetest(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaRetests.ToList().Where(x => x.MachineId == id && x.Years == year).FirstOrDefault();
            if (DAList == null)
            {
                DAList = new ViewDaRetest();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;

            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }
        [HttpGet("GetNoGlove/{id}/{year}")]
        public string GetNoGlove(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaNogloves.ToList().Where(x => x.MachineId == id && x.Years == year).FirstOrDefault();
            if (DAList == null)
            {
                DAList = new ViewDaNoglove();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;

            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }
        [HttpGet("GetTotal/{id}/{year}")]
        public string GetTotal(int id, int year)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaTotals.ToList().Where(x => x.MachineId == id && x.Years == year).FirstOrDefault();
            if (DAList == null)
            {
                DAList = new ViewDaTotal();
                DAList.January = 0;
                DAList.February = 0;
                DAList.March = 0;
                DAList.April = 0;
                DAList.May = 0;
                DAList.June = 0;
                DAList.July = 0;
                DAList.August = 0;
                DAList.September = 0;
                DAList.October = 0;
                DAList.November = 0;
                DAList.December = 0;

            }
            string json = JsonConvert.SerializeObject(DAList);
            return json;
        }

        [HttpGet("GetMeter/{id}")]
        public string GetMeter(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var DAList = unipac_CmuContext.ViewDaMeterrunnings.ToList().Where(x => x.MachineId == id).OrderBy(x => x.Hourly).OrderBy(x => x.Date);
            List<ViewDaMeterrunning> objMeter = new List<ViewDaMeterrunning>();
            foreach(var DA in DAList)
            {
                objMeter.Add(new ViewDaMeterrunning()
                {
                    MachineId = DA.MachineId,
                    Hourly = DA.Hourly,
                    Date = DA.Date,
                    _12volt = DA._12volt,
                    _24volt = DA._24volt,
                    _12amm = DA._12amm,
                    _24amm = DA._24amm,
                    AirPressure = DA.AirPressure,
                    Tempreature = DA.Tempreature
                });
            }
            string json = JsonConvert.SerializeObject(objMeter, objCSetting.JsonFormatdate());
            return json;

        }

        public (Renci.SshNet.SshClient SshClient, uint Port) ConnectSsh(string sshHostName, string sshUserName, string sshPassword = null,
string sshKeyFile = "id_rsa", string sshPassPhrase = null, int sshPort = 5003, string databaseServer = "localhost", int databasePort = 3306)
        {
            //WriteToFile(DateTime.Now + ": SSHClient Processing...");
            // check arguments
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshHostName)} must be specified.", nameof(sshHostName));
            if (string.IsNullOrEmpty(sshHostName))
                throw new ArgumentException($"{nameof(sshUserName)} must be specified.", nameof(sshUserName));
            if (string.IsNullOrEmpty(sshPassword) && string.IsNullOrEmpty(sshKeyFile))
                throw new ArgumentException($"One of {nameof(sshPassword)} and {nameof(sshKeyFile)} must be specified.");
            if (string.IsNullOrEmpty(databaseServer))
                throw new ArgumentException($"{nameof(databaseServer)} must be specified.", nameof(databaseServer));

            // define the authentication methods to use (in order)
            var authenticationMethods = new List<Renci.SshNet.AuthenticationMethod>();
            try
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...KeyFile: " + sshKeyFile);
                if (!string.IsNullOrEmpty(sshKeyFile))
                {
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshUserName: " + sshUserName);
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...sshPassPhrase: " + sshPassPhrase);
                    authenticationMethods.Add(new Renci.SshNet.PrivateKeyAuthenticationMethod(sshUserName,
                        new Renci.SshNet.PrivateKeyFile(sshKeyFile, string.IsNullOrEmpty(sshPassPhrase) ? null : sshPassPhrase)));
                    //WriteToFile(DateTime.Now + ": SSHClient Processing...Done: " + authenticationMethods);
                }
            }
            catch (Exception ex)
            {
                //WriteToFile(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Key: " + ex.InnerException.Message);
            }
            try
            {
                if (!string.IsNullOrEmpty(sshPassword))
                {
                    authenticationMethods.Add(new Renci.SshNet.PasswordAuthenticationMethod(sshUserName, sshPassword));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now + ": SSHClient Processing...Error Password: " + ex.InnerException.Message);
            }

            // connect to the SSH server
            var sshClient = new Renci.SshNet.SshClient(new Renci.SshNet.ConnectionInfo(sshHostName, sshPort, sshUserName, authenticationMethods.ToArray()));
            try
            {
                sshClient.Connect();

                // forward a local port to the database server and port, using the SSH server
                var forwardedPort = new Renci.SshNet.ForwardedPortLocal("127.0.0.1", databaseServer, (uint)databasePort);
                sshClient.AddForwardedPort(forwardedPort);
                forwardedPort.Start();

                return (sshClient, forwardedPort.BoundPort);
            }
            catch (Exception ex)
            {
                return (sshClient, 0);
            }
        }

        [HttpGet("GetYieldCompany/{id}")]
        public Task<string> GetYieldCompanyAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();

            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.CompanyId == id && x.Recordstatus != "Inactive" && x.Recordstatus != "Off").ToList();
            List<DA> objDA = new List<DA>();
            List<TblOpMachineinfo> machinetest = new List<TblOpMachineinfo>();
            foreach (var machines in machineslist)
            {
                var sshServer = machines.Sshipaddress;
                var sshUserName = machines.Sshusername;
                var sshPassword = "";
                var databaseServer = machines.Databaseserver;
                var databaseUserName = machines.Databaseusername;
                var databasePassword = machines.Databasepassword;
                var sshPort = Convert.ToInt32(machines.Sshport);
                var sshKeyFile = "id_rsa";
                var database = machines.Databasename;

                try
                {
                    var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
                    
                    using (sshClient)
                    {
                        MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                        {
                            Server = "127.0.0.1",
                            Port = localPort,
                            UserID = databaseUserName,
                            Password = databasePassword,
                            Database = database
                        };
                        if (localPort > 0)
                        {
                            var connection = new MySqlConnection(csb.ConnectionString);
                            connection.Open();
                            string Query = string.Format(@"SELECT 
                                                    DAY(cEndtime) AS DAY, 
                                                    MONTH(cEndtime) AS MONTH, 
                                                    YEAR(cEndtime) AS Years, 
                                                    SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                    SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                    SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                    SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                    SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                    SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                    FROM tblbatchrecord 
                                                    GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                            MySqlCommand cmd = new MySqlCommand(Query, connection);
                            MySqlDataReader reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                objDA.Add(new DA()
                                {
                                    Day = reader.GetInt32("DAY"),
                                    Month = reader.GetInt32("MONTH"),
                                    Years = reader.GetInt32("Years"),
                                    Pass = reader.GetInt32("Pass"),
                                    Failed = reader.GetInt32("Failed"),
                                    Retest = reader.GetInt32("Retest"),
                                    Reject = reader.GetInt32("Reject"),
                                    NoGlove = reader.GetInt32("NoGlove"),
                                    Total = reader.GetInt32("Total"),
                                });
                            }
                            connection.Close();
                        }
                        //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);

                    }
                    sshClient.Disconnect();
                    sshClient.Dispose();
                }
                catch (Exception ex)
                {

                }
            }
            string json = JsonConvert.SerializeObject(objDA, objCSetting.JsonFormatdate());
            return Task.FromResult(json);
        }

        [HttpGet("GetYield/{id}")]
        public async Task<string> GetYieldAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
            List<DA> objDA = new List<DA>();
            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                        ConnectionTimeout = 3
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    if (localPort > 0) {
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        await connection.OpenAsync();
                        string Query = string.Format(@"SELECT 
                                                    DAY(cEndtime) AS DAY, 
                                                    MONTH(cEndtime) AS MONTH, 
                                                    YEAR(cEndtime) AS Years, 
                                                    SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                    SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                    SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                    SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                    SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                    SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                    FROM tblbatchrecord 
                                                    GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        
                        while (await reader.ReadAsync())
                        {
                            objDA.Add(new DA()
                            {
                                Day = reader.GetInt32("DAY"),
                                Month = reader.GetInt32("MONTH"),
                                Years = reader.GetInt32("Years"),
                                Pass = reader.GetInt32("Pass"),
                                Failed = reader.GetInt32("Failed"),
                                Retest = reader.GetInt32("Retest"),
                                Reject = reader.GetInt32("Reject"),
                                NoGlove = reader.GetInt32("NoGlove"),
                                Total = reader.GetInt32("Total"),
                            });
                        }
                        connection.Close();
                    }
                    string json = JsonConvert.SerializeObject(objDA, objCSetting.JsonFormatdate());
                    return json;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return "";
            }

        }

        [HttpGet("Exceldownload/{id}")]
        public IActionResult Exceldownload(int id)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("User ID,User Name, Activity, Date Time");
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var AuditList = unipac_CmuContext.TblCmAuditlogs.ToList().Where(x => x.UserId == id).OrderByDescending(x => x.CreateDate);
                if (AuditList.Count() > 0)
                {
                    foreach(var Audit in AuditList)
                    {
                        stringBuilder.AppendLine($"{Audit.UserId}, { objCSetting.GetUserName(Audit.UserId) },{ Audit.Activity}, {Audit.CreateDate}");
                    }
                }


                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "ActivityLog.csv");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("ExcelDownloadEvent/{id}")]
        public IActionResult ExcelDownloadEvent(int id)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Date Time, Event");

                var sshServer = machineslist.Sshipaddress;
                var sshUserName = machineslist.Sshusername;
                var sshPassword = "";
                var databaseServer = machineslist.Databaseserver;
                var databaseUserName = machineslist.Databaseusername;
                var databasePassword = machineslist.Databasepassword;
                var sshPort = Convert.ToInt32(machineslist.Sshport);
                var sshKeyFile = "id_rsa";
                var database = machineslist.Databasename;

                var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);

                try
                {
                    using (sshClient)
                    {
                        MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                        {
                            Server = "127.0.0.1",
                            Port = localPort,
                            UserID = databaseUserName,
                            Password = databasePassword,
                            Database = database,
                        };
                        //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        connection.Open();
                        string Query = string.Format(@"SELECT * FROM tbleventlog");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        List<EventLog> objEvent = new List<EventLog>();
                        while (reader.Read())
                        {
                            stringBuilder.AppendLine($"{reader.GetDateTime("cDateTime")}, { reader.GetString("cEvent") }");
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }

                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "EventLog.csv");
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("ExcelDownloadFault/{id}")]
        public IActionResult ExcelDownloadFault(int id)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Date Time, Event, Fault Log");

                var ProductionYieldList = unipac_CmuContext.TblOpMachinefaultlogs.ToList().Where(x => x.MachineId == id);
                if (ProductionYieldList.Count() > 0)
                {
                    foreach (var Production in ProductionYieldList)
                    {
                        stringBuilder.AppendLine($"{Production.CreateDate}, {Production.Event}, {Production.FaultLog}");
                    }
                }


                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "FaultLog.csv");
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("ExceldownloadBatch/{id}")]
        public async Task<IActionResult> ExceldownloadBatchAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
            List<DABatch> objDA = new List<DABatch>();
            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                        ConnectionTimeout = 3
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    if (localPort > 0)
                    {
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        await connection.OpenAsync();
                        string Query = string.Format(@"SELECT 
                                                        DAY(cEndtime) AS DAY, 
                                                        MONTH(cEndtime) AS MONTH, 
                                                        YEAR(cEndtime) AS Years,
                                                        cMachineNo, 
                                                        cRate,
                                                        cOperator1_Name,
                                                        cOperator2_Name,
                                                        cInspector_Name,
                                                        cBatchNo,
                                                        SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                        SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                        SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                        SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                        SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                        SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                        FROM tblbatchrecord 
                                                        GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime), cMachineNo");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (await reader.ReadAsync())
                        {
                            objDA.Add(new DABatch()
                            {
                                Day = reader.GetInt32("DAY"),
                                Month = reader.GetInt32("MONTH"),
                                Years = reader.GetInt32("Years"),
                                Pass = reader.GetInt32("Pass"),
                                Failed = reader.GetInt32("Failed"),
                                Retest = reader.GetInt32("Retest"),
                                Reject = reader.GetInt32("Reject"),
                                NoGlove = reader.GetInt32("NoGlove"),
                                Total = reader.GetInt32("Total"),
                                cMachineNo = reader.GetString("cMachineNo"),
                                cRate = reader.GetString("cRate"),
                                cOperator1_Name = reader.GetString("cOperator1_Name"),
                                cOperator2_Name = reader.GetString("cOperator2_Name"),
                                cInspector_Name = reader.GetString("cInspector_Name"),
                                cBatchNo = reader.GetString("cBatchNo")
                            });
                        }
                        connection.Close();
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("Batch No, Machine No,Rate,Operator 1,Operator 2, Inspector, Pass, Pass(%), Failed, Failed(%), Retest, Retest(%), Reject, Reject(%), No Glove, No Glove(%), Total");
                    foreach (var Production in objDA)
                    {
                        var perPass = Production.Total > 0 ? Convert.ToDecimal(Production.Pass) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perFailed = Production.Total > 0 ? Convert.ToDecimal(Production.Failed) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perRetest = Production.Total > 0 ? Convert.ToDecimal(Production.Retest) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perReject = Production.Total > 0 ? Convert.ToDecimal(Production.Reject) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perNoGlove = Production.Total > 0 ? Convert.ToDecimal(Production.NoGlove) / (Convert.ToDecimal(Production.NoGlove) + Convert.ToDecimal(Production.Total)) * 100 : 0;

                        stringBuilder.AppendLine($"{ Production.cBatchNo },{ Production.cMachineNo },{ Production.cRate}, {Production.cOperator1_Name}, {Production.cOperator2_Name}, {Production.cInspector_Name}, {Production.Pass}, {Math.Round(perPass,2)}, {Production.Failed}, {Math.Round(perFailed,2)},{Production.Retest}, {Math.Round(perRetest,2)}, {Production.Reject}, {Math.Round(perReject,2)}, {Production.NoGlove}, {Math.Round(perNoGlove,2)}, {Production.Total}");
                    }

                    //return Ok(JSONExport);
                    return File(Encoding.UTF8.GetBytes
                    (stringBuilder.ToString()), "text/csv", "ProductionYield.csv");

                }
            }
            catch (Exception ex)
            {
                WriteToFile(DateTime.Now + ": Connectionstring: " + ex.Message);
                return NotFound(ex.Message) ;
            }
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\DownloadFileLogs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\DownloadFileLogs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        [HttpGet("ExceldownloadByMacYield/{id}")]
        public async Task<IActionResult> ExceldownloadByMacYieldAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
            List<DA> objDA = new List<DA>();
            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                        ConnectionTimeout = 3
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    if (localPort > 0)
                    {
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        await connection.OpenAsync();
                        string Query = string.Format(@"SELECT 
                                                    DAY(cEndtime) AS DAY, 
                                                    MONTH(cEndtime) AS MONTH, 
                                                    YEAR(cEndtime) AS Years, 
                                                    SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                    SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                    SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                    SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                    SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                    SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                    FROM tblbatchrecord 
                                                    GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (await reader.ReadAsync())
                        {
                            objDA.Add(new DA()
                            {
                                Day = reader.GetInt32("DAY"),
                                Month = reader.GetInt32("MONTH"),
                                Years = reader.GetInt32("Years"),
                                Pass = reader.GetInt32("Pass"),
                                Failed = reader.GetInt32("Failed"),
                                Retest = reader.GetInt32("Retest"),
                                Reject = reader.GetInt32("Reject"),
                                NoGlove = reader.GetInt32("NoGlove"),
                                Total = reader.GetInt32("Total"),
                            });
                        }
                        connection.Close();
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("Day,Month,Years,Pass, Pass(%), Failed, Failed(%), Retest, Retest(%), Reject, Reject(%), No Glove, No Glove(%), Total");
                    foreach (var Production in objDA)
                    {
                        var perPass = Production.Total > 0 ? Convert.ToDecimal(Production.Pass) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perFailed = Production.Total > 0 ? Convert.ToDecimal(Production.Failed) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perRetest = Production.Total > 0 ? Convert.ToDecimal(Production.Retest) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perReject = Production.Total > 0 ? Convert.ToDecimal(Production.Reject) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perNoGlove = Production.Total > 0 ? Convert.ToDecimal(Production.NoGlove) / (Convert.ToDecimal(Production.NoGlove) + Convert.ToDecimal(Production.Total)) * 100 : 0;

                        stringBuilder.AppendLine($"{ Production.Day },{ Production.Month}, {Production.Years},{Production.Pass}, {Math.Round(perPass, 2)}, {Production.Failed}, {Math.Round(perFailed, 2)},{Production.Retest}, {Math.Round(perRetest, 2)}, {Production.Reject}, {Math.Round(perReject, 2)}, {Production.NoGlove}, {Math.Round(perNoGlove, 2)}, {Production.Total}");
                    }

                    return File(Encoding.UTF8.GetBytes
                    (stringBuilder.ToString()), "text/csv", "ProductionYield.csv");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return null;
            }
        }

        [HttpGet("ExceldownloadYield/{id}")]
        public async Task<IActionResult> ExceldownloadYieldAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.CompanyId == id).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Machine No,Day,Month,Years,Total");
            try
            {
                foreach (var Machine in machineslist)
                {
                    var sshServer = Machine.Sshipaddress;
                    var sshUserName = Machine.Sshusername;
                    var sshPassword = "";
                    var databaseServer = Machine.Databaseserver;
                    var databaseUserName = Machine.Databaseusername;
                    var databasePassword = Machine.Databasepassword;
                    var sshPort = Convert.ToInt32(Machine.Sshport);
                    var sshKeyFile = "id_rsa";
                    var database = Machine.Databasename;

                    var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
                    List<DA> objDA = new List<DA>();
                    using (sshClient)
                    {
                        MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                        {
                            Server = "127.0.0.1",
                            Port = localPort,
                            UserID = databaseUserName,
                            Password = databasePassword,
                            Database = database,
                            ConnectionTimeout = 3
                        };
                        //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                        if (localPort > 0)
                        {
                            using var connection = new MySqlConnection(csb.ConnectionString);
                            await connection.OpenAsync();
                            string Query = string.Format(@"SELECT 
                                                DAY(cEndtime) AS DAY, 
                                                MONTH(cEndtime) AS MONTH, 
                                                YEAR(cEndtime) AS Years, 
                                                SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                FROM tblbatchrecord 
                                                GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                            MySqlCommand cmd = new MySqlCommand(Query, connection);
                            MySqlDataReader reader = cmd.ExecuteReader();

                            while (await reader.ReadAsync())
                            {
                                objDA.Add(new DA()
                                {
                                    Day = reader.GetInt32("DAY"),
                                    Month = reader.GetInt32("MONTH"),
                                    Years = reader.GetInt32("Years"),
                                    Pass = reader.GetInt32("Pass"),
                                    Failed = reader.GetInt32("Failed"),
                                    Retest = reader.GetInt32("Retest"),
                                    Reject = reader.GetInt32("Reject"),
                                    NoGlove = reader.GetInt32("NoGlove"),
                                    Total = reader.GetInt32("Total"),
                                });
                            }
                            connection.Close();
                        }
                    }
                    foreach (var Production in objDA)
                    {
                        var perPass = Production.Total > 0 ? Convert.ToDecimal(Production.Pass) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perFailed = Production.Total > 0 ? Convert.ToDecimal(Production.Failed) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perRetest = Production.Total > 0 ? Convert.ToDecimal(Production.Retest) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perReject = Production.Total > 0 ? Convert.ToDecimal(Production.Reject) / Convert.ToDecimal(Production.Total) * 100 : 0;
                        var perNoGlove = Production.Total > 0 ? Convert.ToDecimal(Production.NoGlove) / (Convert.ToDecimal(Production.NoGlove) + Convert.ToDecimal(Production.Total)) * 100 : 0;

                        stringBuilder.AppendLine($"{Machine.SerialNumber},{Production.Pass}, {Math.Round(perPass, 2)}, {Production.Failed}, {Math.Round(perFailed, 2)},{Production.Retest}, {Math.Round(perRetest, 2)}, {Production.Reject}, {Math.Round(perReject, 2)}, {Production.NoGlove}, {Math.Round(perNoGlove, 2)}, {Production.Total}");
                    }
                }
                return File(Encoding.UTF8.GetBytes(stringBuilder.ToString()), "text/csv", "ProductionYield.csv");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return NotFound(ex.InnerException.Message);
            }
        }

        [HttpGet("ExceldownloadByMacVolume/{id}")]
        public async Task<IActionResult> ExceldownloadByMacVolumeAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.Id == id).FirstOrDefault();

            var sshServer = machineslist.Sshipaddress;
            var sshUserName = machineslist.Sshusername;
            var sshPassword = "";
            var databaseServer = machineslist.Databaseserver;
            var databaseUserName = machineslist.Databaseusername;
            var databasePassword = machineslist.Databasepassword;
            var sshPort = Convert.ToInt32(machineslist.Sshport);
            var sshKeyFile = "id_rsa";
            var database = machineslist.Databasename;

            var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
            List<DA> objDA = new List<DA>();
            try
            {
                using (sshClient)
                {
                    MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                    {
                        Server = "127.0.0.1",
                        Port = localPort,
                        UserID = databaseUserName,
                        Password = databasePassword,
                        Database = database,
                        ConnectionTimeout = 3
                    };
                    //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                    if (localPort > 0)
                    {
                        using var connection = new MySqlConnection(csb.ConnectionString);
                        await connection.OpenAsync();
                        string Query = string.Format(@"SELECT 
                                                    DAY(cEndtime) AS DAY, 
                                                    MONTH(cEndtime) AS MONTH, 
                                                    YEAR(cEndtime) AS Years, 
                                                    SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                    SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                    SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                    SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                    SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                    SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                    FROM tblbatchrecord 
                                                    GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                        MySqlCommand cmd = new MySqlCommand(Query, connection);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (await reader.ReadAsync())
                        {
                            objDA.Add(new DA()
                            {
                                Day = reader.GetInt32("DAY"),
                                Month = reader.GetInt32("MONTH"),
                                Years = reader.GetInt32("Years"),
                                Pass = reader.GetInt32("Pass"),
                                Failed = reader.GetInt32("Failed"),
                                Retest = reader.GetInt32("Retest"),
                                Reject = reader.GetInt32("Reject"),
                                NoGlove = reader.GetInt32("NoGlove"),
                                Total = reader.GetInt32("Total"),
                            });
                        }
                        connection.Close();
                    }
                }

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Day,Month,Years,Total");
                foreach (var Production in objDA)
                {
                    stringBuilder.AppendLine($"{ Production.Day },{ Production.Month}, {Production.Years}, {Production.Total}");
                }


                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "ProductionYield.csv");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return NotFound(ex.InnerException.Message);
            }
        }

        [HttpGet("ExceldownloadVolume/{id}")]
        public async Task<IActionResult> ExceldownloadVolumeAsync(int id)
        {
            unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
            var machineslist = unipac_CmuContext.TblOpMachineinfos.Where(x => x.CompanyId == id).ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Machine No,Day,Month,Years,Total");
            try {
                foreach (var Machine in machineslist)
                {
                    var sshServer = Machine.Sshipaddress;
                    var sshUserName = Machine.Sshusername;
                    var sshPassword = "";
                    var databaseServer = Machine.Databaseserver;
                    var databaseUserName = Machine.Databaseusername;
                    var databasePassword = Machine.Databasepassword;
                    var sshPort = Convert.ToInt32(Machine.Sshport);
                    var sshKeyFile = "id_rsa";
                    var database = Machine.Databasename;

                    var (sshClient, localPort) = ConnectSsh(sshServer, sshUserName, sshPassword, databaseServer: databaseServer, sshPort: sshPort, sshKeyFile: sshKeyFile);
                    List<DA> objDA = new List<DA>();
                    using (sshClient)
                    {
                        MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder
                        {
                            Server = "127.0.0.1",
                            Port = localPort,
                            UserID = databaseUserName,
                            Password = databasePassword,
                            Database = database,
                            ConnectionTimeout = 3
                        };
                        //WriteToFile(DateTime.Now + ": Connectionstring: " + csb.ConnectionString);
                        if (localPort > 0)
                        {
                            using var connection = new MySqlConnection(csb.ConnectionString);
                            await connection.OpenAsync();
                            string Query = string.Format(@"SELECT 
                                                DAY(cEndtime) AS DAY, 
                                                MONTH(cEndtime) AS MONTH, 
                                                YEAR(cEndtime) AS Years, 
                                                SUM(cPassed_Op1 + cPassed_Op2) AS Pass, 
                                                SUM(cRetest_Op1 + cRetest_Op2) AS Retest,
                                                SUM(cFailed_Op1 + cFailed_Op2) AS Failed, 
                                                SUM(cReject_Op1 + cReject_Op2) AS Reject,
                                                SUM(cNoGlove_Op1 + cNoGlove_Op2) AS NoGlove,
                                                SUM(cPassed_Op1 + cPassed_Op2 + cRetest_Op1 + cRetest_Op2 + cFailed_Op1 + cFailed_Op2 + cReject_Op1 + cReject_Op2) AS Total
                                                FROM tblbatchrecord 
                                                GROUP BY MONTH(cEndtime), DAY(cEndtime), YEAR(cEndtime)");
                            MySqlCommand cmd = new MySqlCommand(Query, connection);
                            MySqlDataReader reader = cmd.ExecuteReader();

                            while (await reader.ReadAsync())
                            {
                                objDA.Add(new DA()
                                {
                                    Day = reader.GetInt32("DAY"),
                                    Month = reader.GetInt32("MONTH"),
                                    Years = reader.GetInt32("Years"),
                                    Pass = reader.GetInt32("Pass"),
                                    Failed = reader.GetInt32("Failed"),
                                    Retest = reader.GetInt32("Retest"),
                                    Reject = reader.GetInt32("Reject"),
                                    NoGlove = reader.GetInt32("NoGlove"),
                                    Total = reader.GetInt32("Total"),
                                });
                            }
                            connection.Close();
                        }
                    }
                    foreach (var Production in objDA)
                    {
                        stringBuilder.AppendLine($"{Machine.SerialNumber},{ Production.Day },{ Production.Month}, {Production.Years}, {Production.Total}");
                    }
                }
                return File(Encoding.UTF8.GetBytes(stringBuilder.ToString()), "text/csv", "ProductionYield.csv");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return NotFound(ex.InnerException.Message);
            }
        }

        [HttpGet("MachineOccupany/{id}")]
        public IActionResult MachineOccupany(int id)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("CompanyID,CompanyAddress,CompanyName,Email,MaxAccount,SerialNumber,Location,YearOfManufac,installationdate,calibrationdate,version");
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var machineslist = unipac_CmuContext.ViewOpMachineviewcompanies.ToList().Where(x => x.CompanyId == id);
                foreach (var Machine in machineslist)
                {
                    stringBuilder.AppendLine($"{Machine.CompanyId},{Machine.CompanyAddress},{Machine.CompanyName},{Machine.Email},{Machine.MaxAccount},{Machine.SerialNumber},{Machine.Location},{Machine.YearOfManufac},{Machine.Installationdate},{Machine.Calibrationdate},{Machine.Version}");
                }


                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "ProductionYield.csv");
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
