﻿using CMU_CRUD.DBModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CMU_CRUD.Lib
{
    public class CommonSetting
    {
        public JsonSerializerSettings JsonFormatdate()
        {
            // setup customize formatter for date time converter
            JsonSerializerSettings settings = new JsonSerializerSettings();
            IsoDateTimeConverter dateConverter = new IsoDateTimeConverter
            {
                DateTimeFormat = "dd/MM/yyyy HH:mm:ss"
            };
            settings.Converters.Add(dateConverter);

            return settings;
        }

        public void Auditlog(int UserID, string Activity)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                TblCmAuditlog objAuditlog = new TblCmAuditlog();
                objAuditlog.UserId = UserID;
                objAuditlog.Activity = Activity;
                objAuditlog.CreateDate = DateTime.Now;
                unipac_CmuContext.TblCmAuditlogs.Add(objAuditlog);
                unipac_CmuContext.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public string GetUserName(int UserID)
        {
            try
            {
                unipac_cmuContext unipac_CmuContext = new unipac_cmuContext();
                var UserList = unipac_CmuContext.TblCmUsers.Where(x => x.Id == UserID).Select(x => x.Username);
                return UserList.FirstOrDefault();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public void MailSend(string MessageBody, string Email)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("qmaxv4cmu@unipac.com.my");
                message.To.Add(new MailAddress(Email));
                message.Subject = "Company Registration For CMU";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = MessageBody;
                smtp.Port = 2525;
                smtp.Host = "mail.unipac.com.my";
                smtp.EnableSsl = false;
                //smtp.UseDefaultCredentials = true;
                smtp.Credentials = new NetworkCredential("qmaxv4cmu@unipac.com.my", "DYck@9285");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);

                string Activity = string.Format(@"{0} mail had sent", Email);
                Auditlog(0, Activity);
            }
            catch (Exception ex) {
                string Activity = string.Format(@"{0} mail had sent failed", Email);
                Auditlog(0, Activity);
            }
        }
    }
}
