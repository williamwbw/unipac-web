﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMU_CRUD.Lib
{
    public class ConnectionCustom
    {
        public MySqlConnection CreateConnection(string DBInstance, string DBPort, string DBName, string DBUser, string DBPassword)
        {
            string connString = string.Format(@"server={0};port={1};database={2};user={3};password={4}", DBInstance, DBPort, DBName, DBUser, DBPassword);
            using MySqlConnection connection = new MySqlConnection(connString);
            return connection;
        }

        public void CollectDataPassToMainDB(string FromTable, string ToTable)
        {

        }

        //Format data
        //Insert => Field ~> "ABC, DEF, GRT, TTT, EEE", Data ~> "123, 123, 123, 123, 123"
        //Update => Field ~> 1 Argument Allow, Data ~> 1 Argeumet Allow
        public string MakeQuery(string QueryMode, string Table, string Condition, string Field, string Data)
        {
            try
            {
                string Query = "";
                switch (QueryMode.ToLower())
                {
                    case "insert":
                        Query = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})", Table, Field, Data);
                        break;
                    case "update":
                        Query = string.Format(@"UPDATE {0} SET {1} = {2} ", Table, Field, Data);
                        break;
                    case "delete":
                        Query = string.Format(@"DELETE FROM {0} ", Table);
                        break;
                    case "select":
                        Query = string.Format(@"SELECT * FROM {0} ", Table);
                        break;
                }
                if (Condition != "1=1")
                {
                    Query += string.Format("WHERE {1}", Condition);
                }
                return Query;
            }catch(Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
