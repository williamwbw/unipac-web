﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace FileTransfer
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 60000; //number in milisecinds  
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                string SerialNumber = string.Empty;
                JObject o1 = JObject.Parse(File.ReadAllText(@"C:\WindowsServiceProduction\configuration.json"));

                // read JSON directly from a file
                using (StreamReader file = File.OpenText(@"C:\WindowsServiceProduction\configuration.json"))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    JObject o2 = (JObject)JToken.ReadFrom(reader);
                    SerialNumber = o2["SerialNumber"].ToString();
                    WriteToFile("Service Configuration  " + o2["SerialNumber"]);
                }

                SaveFiles(SerialNumber, "Calibration");
                SaveFiles(SerialNumber, "License");
                SaveSoftware(SerialNumber, "exe");
                SaveSoftware(SerialNumber, "aliases");
                SaveSoftware(SerialNumber, "ini");

                WriteToFile("Service is recall at " + DateTime.Now);
            }
            catch (Exception ex)
            {
                WriteToFile("Error Service " + ex.InnerException.Message);
            }
        }

        public void SaveRFID(string SerialNumber)
        {
            var client = new HttpClient();
            var response = client.GetAsync("http://118.107.242.221:8585/api/Download/GetDownloadSoftware/" + SerialNumber + "/" + type).Result;
            var customerJsonString = response.Content.ReadAsStreamAsync();

            var fileNameResponse = client.GetAsync("http://118.107.242.221:8585/api/Download/GetSoftwareName/" + SerialNumber + "/" + type).Result;
            var strFilename = fileNameResponse.Content.ReadAsStringAsync().Result;

            Task task = response.Content.ReadAsStreamAsync().ContinueWith(t =>
            {
                var stream = t.Result;
                try
                {
                    using (FileStream fileStream = File.Create("C:\\Program Files (x86)\\QMAX4\\UpdatedFile" + strFilename, (int)stream.Length))
                    {
                        byte[] bytesInStream = new byte[stream.Length];
                        stream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                }
                catch (Exception ex)
                {
                    WriteToFile("Error Service " + ex.Message);
                }
            });
        }

        public void SaveSoftware(string SerialNumber, string type)
        {
            var client = new HttpClient();
            var response = client.GetAsync("http://118.107.242.221:8585/api/Download/GetDownloadSoftware/" + SerialNumber + "/" + type).Result;
            var customerJsonString = response.Content.ReadAsStreamAsync();

            var fileNameResponse = client.GetAsync("http://118.107.242.221:8585/api/Download/GetSoftwareName/" + SerialNumber + "/" + type).Result;
            var strFilename = fileNameResponse.Content.ReadAsStringAsync().Result;

            Task task = response.Content.ReadAsStreamAsync().ContinueWith(t =>
            {
                var stream = t.Result;
                try
                {
                    using (FileStream fileStream = File.Create("C:\\Program Files (x86)\\QMAX4\\UpdatedFile" + strFilename, (int)stream.Length))
                    {
                        byte[] bytesInStream = new byte[stream.Length];
                        stream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                }catch(Exception ex)
                {
                    WriteToFile("Error Service " + ex.Message);
                }
            });
        }

        public void SaveFiles(string SerialNumber, string type)
        {
            var client = new HttpClient();
            var response = client.GetAsync("http://118.107.242.221:8585/api/Download/GetDownloadFile/" + SerialNumber + "/" + type).Result;
            var customerJsonString = response.Content.ReadAsStreamAsync();

            var fileNameResponse = client.GetAsync("http://118.107.242.221:8585/api/Download/GetFileName/" + SerialNumber + "/" + type).Result;
            var strFilename = fileNameResponse.Content.ReadAsStringAsync().Result;

            Task task = response.Content.ReadAsStreamAsync().ContinueWith(t =>
            {
                var stream = t.Result;
                try
                {
                    using (FileStream fileStream = File.Create("C:\\Program Files (x86)\\QMAX4\\UpdatedFile" + strFilename, (int)stream.Length))
                    {
                        byte[] bytesInStream = new byte[stream.Length];
                        stream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                }catch(Exception ex)
                {
                    WriteToFile("Error Service " + ex.Message);
                }
            });
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
