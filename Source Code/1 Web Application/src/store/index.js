import { createStore } from "vuex";
import axios from 'axios'
import vue from 'vue'

export default createStore({
  state: {
    //URL Parameter
    APIController: '',
    //Machine Information
    currentcompany: [],
    currentcompanyname: [],
    currentmachine: [],
    machineinfo: [],
    machinestatus: [],
    damachinestatus: [],
    production: [],
    batch: [],
    batchlog: [],
    batchlist: [],
    batchlistwithbatch: [],
    eventlist: [],
    faultlist: [],
    machinelist: [],
    newmachine: [],
    callibrationlist: [],
    licenselist: [],
    //Company Information
    companylist: [],
    //Employee Information
    employeelist: [],
    employeeinfo: [],
    //Setting Information
    productsetting: [],
    newproduct: [],
    newrate: [],
    rate: [],
    //Card Information
    cards: [],
    newcards: [],
    operationlevel: [
      {
        "id": 0,
        "name": "Level 0 - UNIPAC"
      },
      {
        "id": 1,
        "name": "Level 1 - Manager"
      },
      {
        "id": 2,
        "name": "Level 2 - Supervisor"
      },
      {
        "id": 3,
        "name": "Level 3 - Operator"
      }
    ],
    //User Information
    managerlist: [],
    //Generation Information
    gen: [{
      "id": 1,
      "name": "V4"
    },
    {
      "id": 2,
      "name": "V5"
    }],
    versionlist: [],
    //Login Information
    login: [],
    //Access OL
    empOP: [{
      "id": 2,
      "name": "Management"
    },
    {
      "id": 3,
      "name": "Engineering"
    },
    {
      "id": 4,
      "name": "Production"
    }],
    clientadminlog: [],
    clientlog: [],
    //Data Analysis
    Pass: [],
    Fail: [],
    Reject: [],
    Retest: [],
    NoGlove: [],
    Total: [],
    Meter: [],
    Yield: [],
    LocalCMU: []
  },
  mutations: {
    //Machine Information
    SET_Info(state, posts) {
      state.machineinfo = posts
    },
    SET_Status(state, posts) {
      state.machinestatus = posts
    },
    SET_DAStatus(state, posts) {
      state.damachinestatus = posts
    },
    SET_Production(state, posts) {
      state.production = posts
    },
    SET_Batch(state, posts) {
      state.batch = posts
    },
    SET_BatchLog(state, posts) {
      state.batchlog = posts
    },
    SET_BatchList(state, posts) {
      state.batchlist = posts
    },
    SET_EventList(state, posts) {
      state.eventlist = posts
    },
    SET_BatchListWithBatch(state, posts) {
      state.batchlistwithbatch = posts
    },
    SET_ModuleFaultList(state, posts) {
      state.faultlist = posts
    },
    SET_MachineList(state, posts) {
      state.machinelist = posts
    },
    SET_AllMachineList(state, posts) {
      state.allmachinelist = posts
    },
    GET_MachineInfo(state, gets) {
      state.newmachine = gets
    },
    SET_CurrentCompany(state, posts) {
      state.currentcompany = posts
    },
    SET_CurrentCompanyName(state, posts) {
      state.currentcompanyname = posts
    },
    SET_CurrentMachine(state, posts) {
      state.currentmachine = posts
    },
    SET_Callibration(state, posts) {
      state.callibrationlist = posts
    },
    SET_License(state, posts) {
      state.licenselist = posts
    },
    //Company Information
    SET_CompanyInfo(state, posts) {
      state.companylist = posts
    },
    //Employee Information
    SET_EmployeeList(state, posts) {
      state.employeelist = posts
    },
    GET_EmployeeInfo(state, posts) {
      state.employeeinfo = posts
    },
    //Setting Information
    SET_ProductSetting(state, posts) {
      state.productsetting = posts
    },
    SET_RateSetting(state, posts) {
      state.rate = posts
    },
    GET_ProductSetting(state, posts) {
      state.newproduct = posts
    },
    GET_RateSetting(state, posts) {
      state.newrate = posts
    },
    //RFID Informatiom
    SET_CardList(state, posts) {
      state.cards = posts
    },
    Get_Card(state, posts) {
      state.newcards = posts
    },
    //User Information
    SET_ManagerList(state, posts) {
      state.managerlist = posts
    },
    //Version Information
    SET_VersionList(state, posts) {
      state.versionlist = posts
    },
    //Login Information
    GET_Login(state, posts) {
      state.login = posts
      console.log(state.login);
    },
    //Add Client
    GET_Client(state, posts) {
      state.client = posts
      console.log(state.login);
    },
    //Audit Log
    Get_ClientAdminLog(state, posts) {
      state.clientadminlog = posts
    },
    Get_ClientLog(state, posts) {
      state.clientlog = posts
    },
    //Data Analysis
    Get_Pass(state, posts) {
      state.Pass = posts
    },
    Get_Fail(state, posts) {
      state.Fail = posts
    },
    Get_Reject(state, posts) {
      state.Reject = posts
    },
    Get_Retest(state, posts) {
      state.Retest = posts
    },
    Get_NoGlove(state, posts) {
      state.NoGlove = posts
    },
    Get_Total(state, posts) {
      state.Total = posts
    },
    Get_Meter(state, posts) {
      state.Meter = posts
    },
    Get_Yield(state, posts) {
      state.Yield = posts
    },
    GetLocalCMU(state, posts) {
      state.LocalCMU = posts
    }
  },
  getters: {
    Get_StateValue: (state) => (stateGet) => {
      if (stateGet == "newmachine") {
        return state.newmachine;
      } else if (stateGet == "newoperator") {
        return state.employeeinfo;
      } else if (stateGet == "newproduct") {
        return state.newproduct;
      } else if (stateGet == "newrate") {
        return state.newrate;
      } else if (stateGet == "newcard") {
        return state.newcards;
      } else if (stateGet == "login") {
        return state.login;
      } else if (stateGet == "newuser") {
        return state.login;
      } else if (stateGet == "newclient") {
        return state.client;
      } else if (stateGet == "updateClient") {
        return state.client;
      } else if (stateGet == "localcmu") {
        return state.newmachine;
      }
    },
    GET_API: (state) => (stateGet) => {
      state.APIController = 'http://118.107.242.221:8585/api/';
      if (stateGet == "newmachine") {
        return state.APIController += "MachineInfo/SaveMachine";
      } else if (stateGet == "newoperator") {
        return state.APIController += "Employee/SaveEmployee";
      } else if (stateGet == "newproduct") {
        return state.APIController += "Setting/SaveProductSetting";
      } else if (stateGet == "newrate") {
        return state.APIController += "Setting/SaveRateSetting";
      } else if (stateGet == "newcard") {
        return state.APIController += "RFID/SaveRFID";
      } else if (stateGet == "login") {
        return state.APIController += "user/Login";
      } else if (stateGet == "newuser") {
        return state.APIController += "user/CreateUser";
      } else if (stateGet == "newclient") {
        return state.APIController += "Company/CreateUser";
      } else if (stateGet == "updateClient") {
        return state.APIController += "Company/UpdateCompany";
      } else if (stateGet == "localcmu") {
        return state.APIController += "Company/ActivateLocalCMU";
      }
    },
  },
  actions: {
    postAPIwithRandom({ commit, getters }, stateGet) {
      return new Promise((resolve, reject) => {
        axios.post(getters.GET_API(stateGet), getters.Get_StateValue(stateGet), {
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(response => {
            resolve(response.status);
          })
          .catch(error => {
            console.log(error);
            reject(error);
          })
      })
    },
    //Machine Actions
    getMachineInfo({ commit }, machineid) {
      axios.get('http://118.107.242.221:8585/api/MachineInfo/' + machineid)
        .then(response => {
          commit('SET_Info', response.data)
        })
    },
    getMachineList({ commit }, companyid) {
      if (companyid != null && companyid != "") {
        axios.get('http://118.107.242.221:8585/api/MachineInfo/GetMachineWCompany/' + companyid)
          .then(response => {
            commit('SET_MachineList', response.data)
          })
      }
    },
    getAllMachineList({ commit }, companyid) {
      axios.get('http://118.107.242.221:8585/api/MachineInfo/GetAllMachineWCompany/')
        .then(response => {
          commit('SET_AllMachineList', response.data)
        })
    },
    async getMachineStatus({ commit }, machineid) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetStatus/' + machineid)
        .then(response => {
          commit('SET_Status', response.data)
        })
    },
    async getCallibration({ commit }, machineid) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetCallibrationList/' + machineid)
        .then(response => {
          commit('SET_Callibration', response.data)
        })
    },
    async getLicense({ commit }, machineid) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetLicenseList/' + machineid)
        .then(response => {
          commit('SET_License', response.data)
        })
    },
    getDAMachineStatus({ commit }, comID) {
      axios.get('http://118.107.242.221:8585/api/MachineInfo/GetOccupancy/' + comID)
        .then(response => {
          commit('SET_DAStatus', response.data)
        })
    },
    async getProduction({ commit }, machineid) {
      console.log("Production Axios: " + machineid);
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetProduction/' + machineid)
        .then(response => {
          commit('SET_Production', response.data)
        })
    },
    getBatch({ commit }, BatchNo) {
      axios.get('http://118.107.242.221:8585/api/MachineInfo/GetBatch/' + BatchNo)
        .then(response => {
          commit('SET_Batch', response.data)
        })
    },
    getBatchLog({ commit }, BatchNo) {
      if (BatchNo == null || BatchNo == "") {
        BatchNo = "0";
      }
      console.log(BatchNo);
      axios.get('http://118.107.242.221:8585/api/MachineInfo/GetBatch/' + BatchNo)
        .then(response => {
          commit('SET_BatchLog', response.data)
        })
    },
    async getBatchList({ commit }, id) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetBatchList/' + id)
        .then(response => {
          commit('SET_BatchList', response.data)
        })
    },
    getBatchListWithBatch({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/MachineInfo/GetBatchSingle/' + id)
        .then(response => {
          console.log(response.data);
          commit('SET_BatchListWithBatch', response.data)
        })
    },
    async getEventList({ commit }, id) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetEventLog/' + id)
        .then(response => {
          commit('SET_EventList', response.data)
        })
    },
    async getModuleFaultList({ commit }, id) {
      await axios.get('http://118.107.242.221:8585/api/MachineInfo/GetModuleLog/' + id)
        .then(response => {
          commit('SET_ModuleFaultList', response.data)
        })
    },
    //Company Action
    getCompanyInfo({ commit }) {
      axios.get('http://118.107.242.221:8585/api/Company')
        .then(response => {
          commit('SET_CompanyInfo', response.data)
        })
    },
    //Employee Action
    getEmployeeList({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/Employee/' + id)
        .then(response => {
          commit('SET_EmployeeList', response.data)
        })
    },
    //Setting Action
    getProductSetting({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/setting/GetProductSetting/' + id)
        .then(response => {
          commit('SET_ProductSetting', response.data)
        })
    },
    getRateSetting({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/setting/GetRateSetting/' + id)
        .then(response => {
          commit('SET_RateSetting', response.data)
        })
    },
    //RFID Action
    getCardList({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/RFID/GetRFIDList/' + id)
        .then(response => {
          commit('SET_CardList', response.data)
        })
    },
    //User Action
    getManagerList({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/User/GetManagerList/' + id)
        .then(response => {
          commit('SET_ManagerList', response.data)
        })
    },
    //Version Action
    getVersionList({ commit }, id) {
      axios.get('http://118.107.242.221:8585/api/Version/GetVersionList/' + id)
        .then(response => {
          commit('SET_VersionList', response.data)
        })
    },
    //Version Action
    getUserInfo({ commit }, userinfo) {
      console.log("INSIDE: " + getters.Get_StateValue("login"));
      axios.get('http://118.107.242.221:8585/api/User/GetUserInformation/' + userinfo.username + '/' + userinfo.password)
        .then(response => {
          commit('GET_Login', response.data)
        })
    },
    //AuditLog
    getClientAdminLog({ commit }, userid) {
      axios.get('http://118.107.242.221:8585/api/User/GetAuditLog/' + userid)
        .then(response => {
          commit('Get_ClientAdminLog', response.data)
        })
    },
    getClientLog({ commit }, userid) {
      axios.get('http://118.107.242.221:8585/api/User/GetAuditLog/' + userid)
        .then(response => {
          commit('Get_ClientLog', response.data)
        })
    },
    //Data Analsysis
    async GetMeter({ commit }, comid) {
      await axios.get('http://118.107.242.221:8585/api/DA/GetMeter/' + comid)
        .then(response => {
          commit('Get_Meter', response.data)
        })
    },
    async GetYield({ commit }, comid) {
      await axios.get('http://118.107.242.221:8585/api/DA/GetYield/' + comid)
        .then(response => {
          commit('Get_Yield', response.data)
        })
    },
    async GetYieldCompany({ commit }, comid) {
      await axios.get('http://118.107.242.221:8585/api/DA/GetYieldCompany/' + comid)
        .then(response => {
          commit('Get_Yield', response.data)
        })
    },
  },
  modules: {
  },
});
